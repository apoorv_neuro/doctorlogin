//
//  OTPViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 07/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPViewController : UIViewController

@property ( strong , nonatomic ) NSString *doctorId;
@property ( strong , nonatomic ) NSString *mobileNumber;
@end
