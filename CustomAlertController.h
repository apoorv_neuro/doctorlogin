//
//  CustomAlertController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 06/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    
    Simple ,
    Network
    
} AlertType;

@interface CustomAlertController : NSObject

+(void)showAlertOnController:(UIViewController*)controller
               withAlertType:(AlertType)alertType
               andAttributes:(NSDictionary*)attributes;
@end
