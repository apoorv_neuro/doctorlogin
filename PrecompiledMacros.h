//
//  PrecompiledMacros.h
//  Neuronimbus
//
//  Created by Avineet on 20/01/2016.
//  Copyright (c) 2016 Neurnimbus Software Services Pvt. Ltd. All rights reserved.
//

@class AppDelegate;

#define APP_DELEGATE        [UIApplication sharedApplication].delegate

#ifndef W3blog_PrecompiledMacros_h
#define W3blog_PrecompiledMacros_h

// --------------------------
#pragma mark - APP COLORS
// --------------------------

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#pragma mark -

// --------------------------
#pragma mark - Miscellanous
// --------------------------

#define SCREEN_WIDTH        MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)
#define SCREEN_HEIGHT       MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)

#define showViewBorderWithColor(view, color) { view.layer.borderColor = color.CGColor; view.layer.borderWidth = 1.5; }

#define IndexPathZero                       [NSIndexPath indexPathForRow:0 inSection:0]

#define IS_IPHONE_5                         (fabs((double)[[UIScreen mainScreen]bounds].size.height - (double)568) < DBL_EPSILON)

#define IS_IOS_8                            ([[[UIDevice currentDevice] systemVersion] compare:(@"8") options:NSNumericSearch] != NSOrderedAscending)

#define IS_NOT_NIL_OR_NULL(value)           (value != nil && value != Nil && value != NULL && value != (id)[NSNull null])


// --------------------------
#pragma mark - SITE URLs, IPs, and PORTS
// --------------------------

#define SITE_URL                            @"https://logintohealth.com/demo/webservices/"

#endif
