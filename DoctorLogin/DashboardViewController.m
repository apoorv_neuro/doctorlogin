//
//  DashboardViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 09/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "DashboardViewController.h"
#import "DashboardCollectionViewCell.h"
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "CustomAlertController.h"
#import "PersistencyManager.h"
#import <SVProgressHUD.h>
#import "MessageCentreViewController.h"
#import "LiveTileCollectionViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "LiveTilePopupViewController.h"
#import "NotificationViewController.h"
#import "DoctorLogin-Swift.h"
#import "PatientDetailViewController.h"


@interface DashboardViewController () < UICollectionViewDelegate , UICollectionViewDataSource ,UINavigationControllerDelegate ,PopUpDismissProtocol > {
    
    NSArray *collectionArray;
    NotificationViewController *notificationController;
    
    CGRect initialNotificationViewFrame;
}
@property (strong, nonatomic) IBOutlet UIView *leftNavigationItem;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *notificationsHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationsCount;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation DashboardViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    didPan = NO;
    
    _barButtonItem.image = [[UIImage imageNamed:@"nav"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    
    self.navigationController.navigationBar.clipsToBounds = YES;
    
    [self.navigationController.navigationBar setHidden:NO];
    
    
    collectionArray  = @[
                        @{ @"Text":@"Online Consultation\nRequest",
                           @"Image":@"Online-Consultation",
                           @"Background_Color":UIColorFromRGB(0xE96468)
                           },
                        @{ @"Text":@"Missed Calls",
                           @"Image":@"Missed-Calls",
                           @"Background_Color":UIColorFromRGB(0x259EC2)
                           },
                        @{ @"Text":@"Paid\nQuestion Request",
                           @"Image":@"Paid_Request",
                           @"Background_Color":UIColorFromRGB(0x115C71)
                           },
                        @{ @"Text":@"Online Consultation",
                           @"Image":@"Online-Consultation",
                           @"Background_Color":UIColorFromRGB(0x115C71)
                           },
                        @{ @"Text":@"Your\nAppointment",
                           @"Image":@"your_appointment",
                           @"Background_Color":UIColorFromRGB(0x414747)
                           },
                        @{ @"Text":@"Next Available\nGP/Pediatrician call",
                           @"Image":@"Next-Available",
                           @"Background_Color":UIColorFromRGB(0xB52930)
                           
                           },
                        @{ @"Text":@"Your\nNext Consultation",
                           @"Image":@"your_next_consultation",
                           @"Background_Color":UIColorFromRGB(0x1988AB)
                           },
                        @{ @"Text":@"Online Consultation",
                           @"Image":@"Online-Consultation",
                           @"Background_Color":UIColorFromRGB(0x1988AB)
                           },
                        @{ @"Text":@"Online Consultation",
                           @"Image":@"Online-Consultation",
                           @"Background_Color":UIColorFromRGB(0x1988AB)
                           }
                        ];
    
    [self.collectionView setNeedsDisplay];
    
    [self.notificationsHeightConstraint setConstant:0];
    
    // Adding Animations
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                           action:@selector(moveViewWithGestureRecognizer:)];
    [self.viewNotification addGestureRecognizer:panGestureRecognizer];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                action:@selector(didTapNotificationView)];
    [self.viewNotification addGestureRecognizer:tapGesture];
    notificationController = [self.storyboard instantiateViewControllerWithIdentifier:@"notificationScene"];
    
    [self getDashboardData];
}
-(void)viewDidAppear:(BOOL)animated {
    
    // Call Network APIs
    [super viewDidAppear:animated];
    _leftNavigationItem.hidden = NO;
}
#pragma mark - Collection View DataSource Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 8;
}
-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Cells 0,2,5,6 Are Live Tiles
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.dashboardData) {
        
        //  For Live Tiles
        
        LiveTileCollectionViewCell *liveCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"liveTile" forIndexPath:indexPath];
        
        liveCell.labelHeading.text = collectionArray[indexPath.row][@"Text"];
        
        // Case 1 : Individual Success Cases {
        
        if ([appDelegate.dashboardData[@"new_consultation"][0] count] > 0 && indexPath.row == 0) {
            
            [liveCell.imageViewPatient setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://logintohealth.com/demo/assets/uploads/patients/%@",appDelegate.dashboardData[@"new_consultation"][0][@"profile_image"]]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            
            [liveCell setBackgroundColor:collectionArray[indexPath.row][@"Background_Color"]];
            
            liveCell.labelPatient.text = appDelegate.dashboardData[@"new_consultation"][0][@"patient_name"];
            
            if ([appDelegate.dashboardData[@"new_consultation"][0][@"consultation_date"]isEqualToString:@"0000-00-00"]) {
                
                liveCell.labelDate.text = [appDelegate.dashboardData[@"new_consultation"][0][@"period_selected"]uppercaseString];
                
            } else {
                
                liveCell.labelDate.text = appDelegate.dashboardData[@"new_consultation"][0][@"consultation_date"];
            }
            
            return liveCell;
        }
        if ([appDelegate.dashboardData[@"next_paid_question"][0] count] > 0 && indexPath.row == 2) {
            
            [liveCell.imageViewPatient setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://logintohealth.com/demo/assets/uploads/patients/%@",appDelegate.dashboardData[@"next_paid_question"][0][@"profile_image"]]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            
            [liveCell setBackgroundColor:collectionArray[indexPath.row][@"Background_Color"]];
            liveCell.labelPatient.text = appDelegate.dashboardData[@"next_paid_question"][0][@"name"];
            
            liveCell.labelDate.text = appDelegate.dashboardData[@"next_paid_question"][0][@"created"];
            
            return liveCell;
        }
        if ([appDelegate.dashboardData[@"next_consultation"][0] count] > 0 && indexPath.row == 6) {
            
            [liveCell.imageViewPatient setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://logintohealth.com/demo/assets/uploads/patients/%@",appDelegate.dashboardData[@"next_consultation"][0][@"profile_image"]]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            
            [liveCell setBackgroundColor:collectionArray[indexPath.row][@"Background_Color"]];
            liveCell.labelHeading.text = @"Your Next Consultation";
            
            liveCell.labelPatient.text = appDelegate.dashboardData[@"next_consultation"][0][@"patient_name"];
            liveCell.labelDate.text = appDelegate.dashboardData[@"next_consultation"][0][@"consultation_time"];
            
            return liveCell;
        }
        
        // Same For Next_GP (Row 5)
        
        // } Case 1 Ends Here
        
        
        // Case 2 : For Dual Button Cells {
        
        DashboardCollectionViewCell *dualButtonCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell_2"
                                                       
                                                                                                forIndexPath:indexPath];
        
        // 1. Row == 3
        if (indexPath.row == 3) {
            
            [dualButtonCell.bttnLeft setTitle:@"Pending\nPrescriptions" forState:UIControlStateNormal];
            [dualButtonCell.bttnLeft setImage:nil forState:UIControlStateNormal];
            
            [dualButtonCell.bttnLeft.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
            [dualButtonCell.bttnLeft.titleLabel setTextAlignment:NSTextAlignmentCenter];
            [dualButtonCell.bttnLeft setTitleEdgeInsets:UIEdgeInsetsMake(-5, -70, -70, -10)];
            
            [dualButtonCell.bttnRight setTitle:@"Free\nQuestions" forState:UIControlStateNormal];
            [dualButtonCell.bttnRight setImage:nil forState:UIControlStateNormal];
            
            [dualButtonCell.bttnRight.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
            [dualButtonCell.bttnRight.titleLabel setTextAlignment:NSTextAlignmentCenter];
            [dualButtonCell.bttnRight setTitleEdgeInsets:UIEdgeInsetsMake(-5, -70, -60, -10)];
            [dualButtonCell.bttnRight addTarget:self
                                        action:@selector(freeQuestionDetailPressed)
                              forControlEvents:UIControlEventTouchUpInside];
            
            if ([appDelegate.dashboardData[@"count_pending_prescriptions"] isKindOfClass:[NSArray class]]) {
                // Failure Case
                [dualButtonCell.bttnLeft setImage:[UIImage imageNamed:@"Pending"] forState:UIControlStateNormal];
                [dualButtonCell.bttnLeft setImageEdgeInsets:UIEdgeInsetsMake(-40, 5, -6, 5)];
                [dualButtonCell.bttnLeft.imageView setContentMode:UIViewContentModeScaleAspectFit];
                
                
            } else {
                
                UILabel *leftRounded = [[UILabel alloc]initWithFrame:CGRectMake(dualButtonCell.frame.size.width/4-25
                                                                                ,dualButtonCell.frame.size.height/2-20                                                        ,50
                                                                                ,50)];
                leftRounded.layer.masksToBounds = YES;
                leftRounded.layer.cornerRadius = 25;
                
                [leftRounded setTextAlignment:NSTextAlignmentCenter];
                [leftRounded setTextColor:[UIColor whiteColor]];
                [leftRounded setFont:[UIFont boldSystemFontOfSize:20]];
                
                [dualButtonCell addSubview:leftRounded];
                
                [leftRounded setText:[NSString stringWithFormat:@"%@",appDelegate.dashboardData[@"count_pending_prescriptions"]]];
                
                [dualButtonCell.bttnLeft setImage:nil
                                         forState:UIControlStateNormal];
                
                [leftRounded setBackgroundColor:UIColorFromRGB(0xD9E6E7)];
                
                [dualButtonCell.bttnLeft setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, -70, 10)];
                
            }
            
            if ([appDelegate.dashboardData[@"free_questions"][0] count] == 0) {
                
                // Failure Case
                [dualButtonCell.bttnRight setImage:[UIImage imageNamed:@"Free_que"] forState:UIControlStateNormal];
                [dualButtonCell.bttnRight setImageEdgeInsets:UIEdgeInsetsMake(-40, 15, -6, 15)];
                [dualButtonCell.bttnRight.imageView setContentMode:UIViewContentModeScaleAspectFit];
                
            } else {
                // Set Rounded Label Here
                
                UILabel *roundedLabel = [[UILabel alloc]initWithFrame:CGRectMake(dualButtonCell.frame.size.width/1.35-25
                                                                                 ,dualButtonCell.frame.size.height/2-40                                                            ,50
                                                                                 ,50)];

                
                [roundedLabel setTextColor:[UIColor whiteColor]];
                [roundedLabel setFont:[UIFont boldSystemFontOfSize:20]];
                roundedLabel.layer.cornerRadius = 25;
                roundedLabel.layer.masksToBounds = YES;
                [roundedLabel setTextAlignment:NSTextAlignmentCenter];
                
                [dualButtonCell addSubview:roundedLabel];
                //D9E6E7
                [roundedLabel setText:[NSString stringWithFormat:@"%lu",[appDelegate.dashboardData[@"free_questions"][0] count]]];
                
                [roundedLabel setBackgroundColor:UIColorFromRGB(0xC4CECE)];
                [dualButtonCell.bttnRight setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, -70, 0)];
            }
            
            return dualButtonCell;
        }
        // 2. Row == 7
        
        if (indexPath.row == 7) {
            
            // Consultation Request
            
            [dualButtonCell.bttnLeft setTitle:@"Consultation Request" forState:UIControlStateNormal];
            [dualButtonCell.bttnLeft setImage:nil forState:UIControlStateNormal];
            
            [dualButtonCell.bttnLeft setTitleEdgeInsets:UIEdgeInsetsMake(-5, -105, -80, -15)];
            [dualButtonCell.bttnLeft.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
            [dualButtonCell.bttnLeft.titleLabel setTextAlignment:NSTextAlignmentCenter];
            [dualButtonCell.bttnLeft addTarget:self
                                        action:@selector(onlineConsultationDetailPressed)
                              forControlEvents:UIControlEventTouchUpInside];
            
            // Paid Questions
            
            [dualButtonCell.bttnRight setTitle:@"Paid\nQuestion\nRequest" forState:UIControlStateNormal];
            [dualButtonCell.bttnRight setImage:nil forState:UIControlStateNormal];
            [dualButtonCell.bttnRight.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
            [dualButtonCell.bttnRight.titleLabel setTextAlignment:NSTextAlignmentCenter];
            [dualButtonCell.bttnRight setTitleEdgeInsets:UIEdgeInsetsMake(-5, -70, -60, -10)];
            [dualButtonCell.bttnRight addTarget:self
                               action:@selector(paidQuestionDetailPressed)
                     forControlEvents:UIControlEventTouchUpInside];

            
            if ([appDelegate.dashboardData[@"count_my_questions"] isKindOfClass:[NSArray class]]) {
                // Failure
                [dualButtonCell.bttnRight setImage:[UIImage imageNamed:@"Paid_Request"] forState:UIControlStateNormal];
                [dualButtonCell.bttnRight setImageEdgeInsets:UIEdgeInsetsMake(-40, 15, -6, 15)];
                [dualButtonCell.bttnRight.imageView setContentMode:UIViewContentModeScaleAspectFit];
            } else {
                
                UILabel *roundedLabel = [[UILabel alloc]initWithFrame:CGRectMake(dualButtonCell.frame.size.width/1.35-25
                                                                                 ,dualButtonCell.frame.size.height/2-40                                                            ,50
                                                                                 ,50)];
                
                [roundedLabel setTextColor:[UIColor whiteColor]];
                [roundedLabel setFont:[UIFont boldSystemFontOfSize:20]];
                roundedLabel.layer.cornerRadius = 25;
                roundedLabel.layer.masksToBounds = YES;
                [roundedLabel setTextAlignment:NSTextAlignmentCenter];

                [dualButtonCell addSubview:roundedLabel];
                //D9E6E7
                [roundedLabel setText:[NSString stringWithFormat:@"%@",appDelegate.dashboardData[@"count_my_questions"]]];
                
                [roundedLabel setBackgroundColor:UIColorFromRGB(0xC4CECE)];
                [dualButtonCell.bttnRight setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, -70, 0)];
            }
            
            if ([appDelegate.dashboardData[@"count_new_consultations"] isKindOfClass:[NSArray class]]) {
                // Failure
                [dualButtonCell.bttnLeft setImage:[UIImage imageNamed:@"Online-Consultation"] forState:UIControlStateNormal];
                [dualButtonCell.bttnLeft setImageEdgeInsets:UIEdgeInsetsMake(-25, 5, -15, 5)];
                [dualButtonCell.bttnLeft.imageView setContentMode:UIViewContentModeScaleAspectFit];
                
            } else {
                
                UILabel *leftRounded = [[UILabel alloc]initWithFrame:CGRectMake(dualButtonCell.frame.size.width/4-25
                                                                                ,dualButtonCell.frame.size.height/2-40                                                            ,50
                                                                                ,50)];
                leftRounded.layer.masksToBounds = YES;
                leftRounded.layer.cornerRadius = 25;
                
                [leftRounded setTextAlignment:NSTextAlignmentCenter];
                [leftRounded setTextColor:[UIColor whiteColor]];
                [leftRounded setFont:[UIFont boldSystemFontOfSize:20]];
                
                [dualButtonCell addSubview:leftRounded];
                
                [leftRounded setText:[NSString stringWithFormat:@"%@",appDelegate.dashboardData[@"count_new_consultations"]]];
                
                [leftRounded setBackgroundColor:UIColorFromRGB(0xD9E6E7)];
                
                [dualButtonCell.bttnLeft setTitleEdgeInsets:UIEdgeInsetsMake(0, -15, -70, -15)];
                
            }
            return dualButtonCell;
        }
        
        // } Case 2 Ends Here
        
        // For Normal Tiles
        
        DashboardCollectionViewCell *countCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell_1"
                                                                                                forIndexPath:indexPath];
        
        [countCell.activityIndicator stopAnimating];
        [countCell.visualEffectView setHidden:YES];
        
        [countCell.bttnDashboard setImage:[UIImage imageNamed:collectionArray[indexPath.row][@"Image"]]
                                 forState:UIControlStateNormal];
        
        countCell.labelText.text = collectionArray[indexPath.row][@"Text"];
        
        [countCell setBackgroundColor:collectionArray[indexPath.row][@"Background_Color"]];
        
        if (indexPath.row == 1 && ![appDelegate.dashboardData[@"count_missed_calls"]isKindOfClass:[NSArray class]]) {
            
            [countCell.bttnDashboard setBackgroundColor:UIColorFromRGB(0x83D0E1)];
            
            [countCell.bttnDashboard setFrame:CGRectMake(countCell.frame.size.width/2-30
                                                         ,countCell.frame.size.height/2-40                                                            ,60
                                                         ,60)];
            
            countCell.bttnDashboard.layer.cornerRadius = 30;
            
            [countCell.bttnDashboard setImage:nil
                                     forState:UIControlStateNormal];
            
            [countCell.bttnDashboard setTitleColor:[UIColor whiteColor]
                                          forState:UIControlStateNormal];
            
            [countCell.bttnDashboard.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
            
            [countCell.bttnDashboard setTitle : [NSString stringWithFormat:@"%@",appDelegate.dashboardData[@"count_missed_calls"]]
                                     forState : UIControlStateNormal];
        }
        return countCell;
        
    } else {
        
        if (indexPath.row == 3 || indexPath.row == 7) {
            
            DashboardCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier : @"Cell_2"
                                                                                          forIndexPath : indexPath];
            if (indexPath.row == 3) {
                
                // Pending Prescriptions
                
                [cell.bttnLeft setTitle:@"Pending\nPrescriptions" forState:UIControlStateNormal];
                [cell.bttnLeft setImage:[UIImage imageNamed:@"Pending"] forState:UIControlStateNormal];
                
                [cell.bttnLeft.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
                [cell.bttnLeft.titleLabel setTextAlignment:NSTextAlignmentCenter];
                
                [cell.bttnLeft setTitleEdgeInsets:UIEdgeInsetsMake(-5, -70, -70, -10)];
                [cell.bttnLeft setImageEdgeInsets:UIEdgeInsetsMake(-40, 5, -6, 5)];
                [cell.bttnLeft.imageView setContentMode:UIViewContentModeScaleAspectFit];
                
                //Free Questions
                
                [cell.bttnRight setTitle:@"Free\nQuestions" forState:UIControlStateNormal];
                
                [cell.bttnRight setImage:[UIImage imageNamed:@"Free_que"] forState:UIControlStateNormal];
                
                [cell.bttnRight.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
                [cell.bttnRight.titleLabel setTextAlignment:NSTextAlignmentCenter];
                [cell.bttnRight setTitleEdgeInsets:UIEdgeInsetsMake(-5, -70, -60, -10)];
                [cell.bttnRight setImageEdgeInsets:UIEdgeInsetsMake(-40, 15, -6, 15)];
                [cell.bttnRight.imageView setContentMode:UIViewContentModeScaleAspectFit];
                
            } else {
                
                // Consultation Request
                
                [cell.bttnLeft setTitle:@"Consultation Request" forState:UIControlStateNormal];
                [cell.bttnLeft setImage:[UIImage imageNamed:@"Online-Consultation"] forState:UIControlStateNormal];
                
                [cell.bttnLeft.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
                [cell.bttnLeft.titleLabel setTextAlignment:NSTextAlignmentCenter];
                
                [cell.bttnLeft setTitleEdgeInsets:UIEdgeInsetsMake(-5, -105, -80, -15)];
                [cell.bttnLeft setImageEdgeInsets:UIEdgeInsetsMake(-25, 5, -15, 5)];
                [cell.bttnLeft.imageView setContentMode:UIViewContentModeScaleAspectFit];
                
                [cell.bttnLeft addTarget:self
                                  action:@selector(onlineConsultationDetailPressed)
                        forControlEvents:UIControlEventTouchUpInside];
                // Paid Questions
                
                [cell.bttnRight setTitle:@"Paid\nQuestion\nRequest" forState:UIControlStateNormal];
                [cell.bttnRight setImage:[UIImage imageNamed:@"Paid_Request"] forState:UIControlStateNormal];
                [cell.bttnRight.titleLabel setFont:[UIFont systemFontOfSize:13 weight:0.35]];
                [cell.bttnRight.titleLabel setTextAlignment:NSTextAlignmentCenter];
                [cell.bttnRight setTitleEdgeInsets:UIEdgeInsetsMake(-5, -70, -60, -10)];
                [cell.bttnRight setImageEdgeInsets:UIEdgeInsetsMake(-40, 15, -6, 15)];
                [cell.bttnRight.imageView setContentMode:UIViewContentModeScaleAspectFit];
        }
            return cell;
            
        } else {
            
            return [self setBasicCellWithIndexPath:indexPath];
        }
    }
}
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0;
}
- (CGSize)collectionView:(UICollectionView*)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (self.view.frame.size.height < 500) {
        
        return CGSizeMake(self.view.frame.size.width/2, 120);
        
    }
    return CGSizeMake(self.view.frame.size.width/2, self.collectionView.frame.size.height/4);
}

#pragma mark collection view cell paddings

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake( 0
                            ,0
                            ,0
                            ,0);
}

#pragma mark - Collection View Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    switch (indexPath.row) {
            
        case 0:{
            
            
            if ([appDelegate.dashboardData[@"new_consultation"][0] count] == 0) {
                
                [self showMessageCenterWithSelectedIndex:0];
                
            } else {
                
                // Show Pop-Up Here
                
                [self showLiveTilePopUpWithIndexPath:indexPath];
            }
        }
            break;
            
        case 1:{
            
             [self showMessageCenterWithSelectedIndex:3];
            
        }break;
            
        case 2: {
            
            if ([appDelegate.dashboardData[@"next_paid_question"][0] count] > 0) {
                
                [self showLiveTilePopUpWithIndexPath:indexPath];
                
            } else {
                
                [self showMessageCenterWithSelectedIndex:2];
            }
        }
            break;
        default:
            break;
    }
}

// doctor_dashboard

#pragma mark - Calling API's

-(void)getDashboardData {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentInternetStatus) {
        
        NSString *urlAsString=[NSString stringWithFormat:@"https://logintohealth.com/demo/webservices/appdoctorOther/doctor_dashboard"];
        
        NSLog(@"Dashboard API ----> %@",urlAsString);
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *parameters = @{@"doctor_id" : [PersistencyManager getDoctorId] ,
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        
        [manager.requestSerializer setTimeoutInterval:10];
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     appDelegate.dashboardData = responseObject;
                     
                     NSLog(@"Dashboard API Response ----> %@",appDelegate.dashboardData);
                     
                     if ([appDelegate.dashboardData[@"notifications"] count] > 0) {
                         
                         _lblNotificationsCount.text = [NSString stringWithFormat:@"%u",[appDelegate.dashboardData[@"notifications"][0] count]];
                         
                         [self.notificationsHeightConstraint setConstant:50];
                         
                         [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.6 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseIn animations:^{
                             
                             [self.view layoutIfNeeded]; // Called on parent view
                             
                         } completion:nil];
                         
                     } else {
                         
                         [self.notificationsHeightConstraint setConstant:0];
                     }
                 }
                 [self.collectionView reloadData];
                 
                 [[NSNotificationCenter defaultCenter]postNotificationName:@"updateSlideoutMenu" object:nil];
                 
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 NSLog(@" Dashboard API --- Error |||_______---->>>%@" , [error localizedDescription]);
                 
                 [self.collectionView reloadData];
             }];
    }
}

-(DashboardCollectionViewCell*)setBasicCellWithIndexPath:(NSIndexPath*)indexPath {
    
    DashboardCollectionViewCell *countCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell_1"
                                                                                            forIndexPath:indexPath];
    
    [countCell.bttnDashboard setImage:[UIImage imageNamed:collectionArray[indexPath.row][@"Image"]]
                             forState:UIControlStateNormal];
    
    if (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 5 || indexPath.row == 6) {
        
        [countCell.activityIndicator startAnimating];
        [countCell.visualEffectView setHidden:NO];
        
    } else {
        
        [countCell.visualEffectView setHidden:YES];
    }
    
    countCell.labelText.text = collectionArray[indexPath.row][@"Text"];
    [countCell setBackgroundColor:collectionArray[indexPath.row][@"Background_Color"]];
    
    return countCell;
}

#pragma mark - Custom Selector Methods

-(void)onlineConsultationDetailPressed {
    
    [self showMessageCenterWithSelectedIndex:0];
}
-(void)freeQuestionDetailPressed {
    
    [self showMessageCenterWithSelectedIndex:1];

}
-(void)paidQuestionDetailPressed {
    
    [self showMessageCenterWithSelectedIndex:2];
    
}
-(void)showMessageCenterWithSelectedIndex:(NSInteger)selectedIndex {
    
    MessageCentreViewController *messageCenter = [self.storyboard instantiateViewControllerWithIdentifier:@"messageCenter"];
    
    messageCenter.selectedIndex = selectedIndex;
    
    [self.navigationController pushViewController : messageCenter
                                         animated : YES];
}

#pragma mark - Showing Interactive Pop-Ups

-(void)showLiveTilePopUpWithIndexPath:(NSIndexPath*)indexPath {
    
    LiveTilePopupViewController *tilePopupController = [self.storyboard instantiateViewControllerWithIdentifier:@"tilePopUp"];
    
    tilePopupController.initialFrame = [[_collectionView cellForItemAtIndexPath:indexPath] frame];
    tilePopupController.providesPresentationContextTransitionStyle = YES;
    tilePopupController.definesPresentationContext = YES;
    
    [tilePopupController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    tilePopupController.delegate = self;
    
    switch (indexPath.row) {
            
        case 0:
            tilePopupController.popUpContentMode = OnlineConsultation;
            break;
        case 2:
            tilePopupController.popUpContentMode = PaidQuestion;
            break;
        default:
            break;
    }
    
    [self presentViewController:tilePopupController
                       animated:NO
                     completion:nil];
    
}

#pragma mark - Notification Gesture Handler

-(void)moveViewWithGestureRecognizer:(UITapGestureRecognizer *)panGestureRecognizer {
    
    [self initializeNotificationView];
    
    CGPoint touchLocation = [panGestureRecognizer locationInView:self.view];
    
    self.viewNotification.center = CGPointMake(self.viewNotification.center.x
                                               ,touchLocation.y);
    
    if (touchLocation.y > 44) {
        
        CGFloat alphaRatio = (touchLocation.y)/self.view.frame.size.height;
        
        [self.viewNotification setAlpha:alphaRatio];
    }
    
    [notificationController.view setFrame:CGRectMake( 0.0
                                                     ,self.viewNotification.frame.origin.y+self.viewNotification.frame.size.height
                                                     ,self.view.frame.size.width
                                                     ,self.view.frame.size.height)];
    
    if(panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
        
        if (touchLocation.y > self.view.frame.size.height/2) {
            
            [self moveNotificationControllerToInitialPosition];
        }
        if (touchLocation.y < self.view.frame.size.height/2) {
            
            [self moveNotificationControllerToFinalPosition];
        }
    }
}

#pragma mark - Notification View Animations

-(void)moveNotificationControllerToInitialPosition {
    
    [UIView animateWithDuration:0.5
                          delay:0.0
         usingSpringWithDamping:1
          initialSpringVelocity:0.1
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         _viewNotification.frame = initialNotificationViewFrame;
                         
                         [self.viewNotification setAlpha:1];
                         
                         [notificationController.view setFrame:CGRectMake( 0.0
                                                                          ,self.view.frame.size.height
                                                                          ,self.view.frame.size.width
                                                                          ,self.view.frame.size.height)];
                     } completion:^(BOOL finished) {
                         
                         _barButtonItem.image = [[UIImage imageNamed:@"nav"]
                                                 imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                     }];
}
-(void)moveNotificationControllerToFinalPosition {
    
    [UIView animateWithDuration : 0.5
                          delay : 0.0
         usingSpringWithDamping : 1
          initialSpringVelocity : 0.1
                        options : UIViewAnimationOptionCurveEaseIn
                      animations:^{
                          
                          [self.viewNotification setAlpha:0];
                          [notificationController.view setFrame:CGRectMake( 0.0
                                                                           ,64
                                                                           ,self.view.frame.size.width
                                                                           ,self.view.frame.size.height)];
                          
                      } completion:^(BOOL finished) {
                          
                          _barButtonItem.image = [[UIImage imageNamed : @"close"]
                                                  imageWithRenderingMode : UIImageRenderingModeAlwaysTemplate];
                      }];
}
-(void)didTapNotificationView {
    
    [self initializeNotificationView];
    [self moveNotificationControllerToFinalPosition];
}
-(void)initializeNotificationView {
    
    if (!didPan) {
        
        didPan = YES;
        
        initialNotificationViewFrame = _viewNotification.frame;
        
        [notificationController.view setFrame:CGRectMake(0.0,self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        
        [self addChildViewController:notificationController];
        
        [self.view addSubview:notificationController.view];
        
        [notificationController didMoveToParentViewController:self];
    }
}
#pragma mark - Slideout Menu Toggle Nav Bar Button Selector

- (IBAction)bttnNavBarPressed:(id)sender {
    
    if (self.viewNotification.alpha == 0) {
        
        [UIView animateWithDuration:0.5
                              delay:0.0
             usingSpringWithDamping:1
              initialSpringVelocity:0.1
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             [self moveNotificationControllerToInitialPosition];
                         }
                         completion:nil];
    } else {
        
        [self.navigationController toggleSideMenuView];
    }
}

#pragma  mark - PopUp Dismiss Protocol

-(void)popUpDidDismiss:(ContentMode)contentMode {
    
    PatientDetailViewController *patientDetailController = [self.storyboard instantiateViewControllerWithIdentifier:@"patientDetailScene"];
    
    patientDetailController.contentMode = contentMode;
    [self.navigationController pushViewController : patientDetailController
                                         animated : YES];
}
-(void)showSchedulePopUp {
    
    SchedulePopUpViewController *scheduleViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"schedulePopUp"];
    scheduleViewController.providesPresentationContextTransitionStyle = YES;
    
    scheduleViewController.definesPresentationContext = YES;
    [scheduleViewController setModalPresentationStyle : UIModalPresentationOverCurrentContext];
    
    [self presentViewController : scheduleViewController
                       animated : YES
                     completion : nil];
}
// Refresh Data 
-(void)refreshData {
    
    [self getDashboardData];

}

@end