//
//  GuideViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 28/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "GuideViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"

@interface GuideViewController ()
{
    NSArray *array;
    LoginViewController *loginVc;
    RegisterViewController *regVc;
}
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;

@property (weak, nonatomic) IBOutlet UILabel *lblSubHeader;
@property (weak, nonatomic) IBOutlet UILabel *LblBottom;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewSubHeader;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewHeader;

@property (weak, nonatomic) IBOutlet UIView *viewBottom;

@end
@implementation GuideViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    array = @[
              @{@"Header_Img":@"header_1",
                @"Header_Lbl":@"Online Consultation",
                @"SubHeader_Img":@"subHeader_1",
                @"SubHeader_subLbl":@"Phone And Video Consultations"
                },
              
              @{@"Header_Img":@"header_2",
                @"Header_Lbl":@"Ask A Question",
                @"SubHeader_Img":@"subHeader_2",
                @"SubHeader_subLbl":@"Provide Answers To Health Queries"
                },
              
              @{@"Header_Img":@"header_3",
                @"Header_Lbl":@"Clinic Appointment",
                @"SubHeader_Img":@"subHeader_3",
                @"SubHeader_subLbl":@"Share Your Calendar For Instant Clinic Visits"
                },
              
              @{@"Header_Img":@"header_4",
                @"Header_Lbl":@"Diagnostic Test",
                @"SubHeader_Img":@"subHeader_4",
                @"SubHeader_subLbl":@"Provide Quick Advice To Your Existing Patients By Phone"
                },
              
              @{@"Header_Img":@"header_5",
                @"SubHeader_Img":@"subHeader_5",
                @"SubHeader_subLbl":@"Integrated Calendar For Online And Clinic Visits"
                },
              
              @{@"Header_Img":@"header_6",
                @"SubHeader_Img":@"subHeader_6",
                @"SubHeader_subLbl":@"Electronic Medical Records For Patient Care"
                },
              
              @{@"Header_Img":@"header_7",
                @"SubHeader_Img":@"subHeader_7",
                @"SubHeader_subLbl":@"Instant Notifications - SMS And Emails"
                },
              
              @{@"Header_Img":@"header_8",
                @"SubHeader_Img":@"subHeader_8",
                @"SubHeader_subLbl":@"Comprehensive Billing And Collection"
                }
              ];
    
    if (_index == 8) {
       
        // Reset Index For Page Controller
        
        _index = 0;
    }
    
    _imageViewHeader.image = [UIImage imageNamed:array[_index][@"Header_Img"]];
    
    if (_index >= 4) {
        
        // For Blue Pattern Images
        
        [_viewHeader setHidden:YES];
        [_lblHeader setHidden:YES];
        
        _lblSubHeader.text = @"Manage Your Clinic Practice Effortlessly With Our Integrated Digital Platform";
        
        [_pageControl setCurrentPage:_index - 4];
        [_viewBottom setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_part"]]];
        
    } else {
        
        // For Red Pattern Images
        
        [_viewHeader setHidden:NO];
        [_lblHeader setHidden:NO];
        
        _lblSubHeader.text = @"Grow Your Existing Practice With Our Online Practice Platform And Services";
        
        NSString *headerLabelText = array[_index][@"Header_Lbl"];
        _lblHeader.text = headerLabelText.uppercaseString;
        
        [_pageControl setCurrentPage:_index];
        [_viewBottom setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"red_part"]]];
    }
    
    _LblBottom.text = array[_index][@"SubHeader_subLbl"];
    _imageViewSubHeader.image = [UIImage imageNamed:array[_index][@"SubHeader_Img"]];
}

#pragma mark - Segmented Control Action Method

- (IBAction)buttonSegmentedControlPressed:(id)sender {
    
    UISegmentedControl *control = (UISegmentedControl*)sender;
    
    if (control.selectedSegmentIndex == 0) {
        
        loginVc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginScene"];

        [self presentViewController:loginVc
                           animated:YES
                         completion:nil];
    }
    if (control.selectedSegmentIndex == 1) {
       
        regVc = [self.storyboard instantiateViewControllerWithIdentifier:@"registerScene"];

        [self presentViewController:regVc
                           animated:YES
                         completion:nil];
    }
}
@end
