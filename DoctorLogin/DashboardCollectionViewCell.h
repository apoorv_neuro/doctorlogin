//
//  DashboardCollectionViewCell.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 10/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *bttnDashboard;
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@property (weak, nonatomic) IBOutlet UIButton *bttnLeft;
@property (weak, nonatomic) IBOutlet UIButton *bttnRight;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *visualEffectView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
