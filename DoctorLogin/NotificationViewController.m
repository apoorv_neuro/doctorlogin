//
//  NotificationViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 04/06/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "NotificationViewController.h"
#import "AppDelegate.h"
#import "DashboardViewController.h"

@interface NotificationViewController () < UITableViewDataSource , UITableViewDelegate > {
    
}
@property (weak  , nonatomic) IBOutlet UIButton *bttnClose;
@property (weak  , nonatomic) IBOutlet UITableView *tableView;
@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view bringSubviewToFront:_bttnClose];
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 40;
}
- (IBAction)buttonClosePressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    AppDelegate *appdelegate = [UIApplication sharedApplication].delegate;

    return [appdelegate.dashboardData[@"notifications"][0] count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    AppDelegate *appdelegate = [UIApplication sharedApplication].delegate;
    
    cell.textLabel.text = appdelegate.dashboardData[@"notifications"][0][indexPath.row][@"message"];
    
    return cell;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    AppDelegate *appdelegate = [UIApplication sharedApplication].delegate;
    
    // Configuring Header View
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake( 0
                                                                  ,0
                                                                  ,self.tableView.frame.size.width
                                                                  ,50)];
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake( 8
                                                                 ,0
                                                                 ,self.tableView.frame.size.width-16
                                                                 ,50)];
    
    [headerView setBackgroundColor:UIColorFromRGB(0x414747)];
    headerView.layer.cornerRadius = 5;
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(14, 13, 108, 23)];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont systemFontOfSize:19]];
    titleLabel.text = @"Notifications";
    [headerView addSubview:titleLabel];
    
    UILabel *notificationCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(headerView.frame.size.width-40
                                                                               ,9
                                                                               ,30
                                                                               ,30)];
    
    [notificationCountLabel setBackgroundColor:[UIColor whiteColor]];
    [notificationCountLabel setTextColor:UIColorFromRGB(0x717273)];
    
    [notificationCountLabel setFont:[UIFont systemFontOfSize:19]];
    notificationCountLabel.text = [NSString stringWithFormat:@"%u",[appdelegate.dashboardData[@"notifications"][0] count]];
    [notificationCountLabel setTextAlignment:NSTextAlignmentCenter];
    notificationCountLabel.layer.cornerRadius = 15;
    notificationCountLabel.layer.masksToBounds = YES;

    [headerView addSubview:notificationCountLabel];
    [contentView addSubview:headerView];
    
    return contentView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 50;
}
@end
