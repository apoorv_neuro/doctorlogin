//
//  PatientDetailViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 14/06/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveTilePopupViewController.h"

@interface PatientDetailViewController : UIViewController

@property (nonatomic) ContentMode contentMode;
@end
