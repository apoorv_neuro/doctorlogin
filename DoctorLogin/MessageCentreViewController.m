//
//  MessageCentreViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 23/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "MessageCentreViewController.h"
#import "MessageCenterTableViewCell.h"
#import "AppDelegate.h"
#import <AFNetworking.h>
#import "PersistencyManager.h"
#import <SVProgressHUD.h>
#import "CustomAlertController.h"
#import "MessageTimeSlotsTableViewCell.h"                  
#import "MessageCenterDetailViewController.h"
#import "HelperClass.h"
#import "ReplyQuestionViewController.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface MessageCentreViewController () < UITableViewDataSource , UITableViewDelegate > {
    
    NSArray *staticHeaderArray;
    
    NSArray *freeQuestionArray;
    
    NSArray *paidQuestionsArray;
    
    UIRefreshControl *refreshControl;
    
    NSMutableIndexSet *expandedRows;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
@implementation MessageCentreViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    expandedRows = [NSMutableIndexSet new];
    
    staticHeaderArray = @[@"New Consultation Request"
                          ,@"Confirmed Consultations"
                          ,@"Previous Consultations"
                          ,@"Cancelled Consultations"
                          ,@"Next Avail GP"];
    
    [_segmentedControl setSelectedSegmentIndex:_selectedIndex];
    
    [self segmentedControlPressed:_segmentedControl];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(getData) forControlEvents:UIControlEventValueChanged];
    
    for (NSString* family in [UIFont familyNames])
    {
        NSLog(@"%@", family);
        
        for (NSString* name in [UIFont fontNamesForFamilyName: family])
        {
            NSLog(@"  %@", name);
        }
    }
}

#pragma mark - UITableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tabmleView {
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:
            return 1;
            break;
            
        case PaidQuestions:
            return 1;
            break;
            
        case FreeQuestions:
            return 1;
            break;
            
        case MissedCalls:
            
            return 1;
            break;
    }
    return 0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:
            return staticHeaderArray.count;
        case PaidQuestions:
            return paidQuestionsArray.count > 0 ? paidQuestionsArray.count : 0;
        case FreeQuestions:
            return freeQuestionArray.count > 0 ? freeQuestionArray.count : 0;
        case MissedCalls:
            return 0;
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations : {
            
            MessageCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"Cell" forIndexPath:indexPath];
            
            cell.labelTitle.text = staticHeaderArray[indexPath.row];
            
            [cell setSelectionStyle : UITableViewCellSelectionStyleNone];
            
            return cell;
        }
        case FreeQuestions : {
            
            MessageTimeSlotsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"questionCell"
                                                                                  forIndexPath:indexPath];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                         action:@selector(freeQuestionContainerTouched:)];
            
            [cell.containerView setTag:indexPath.row];
            
            [cell.bttnReplyNow addTarget:self
                                  action:@selector(replyNowTouched:)
                        forControlEvents:UIControlEventTouchUpInside];
            
            [cell.bttnReplyNow setTag:indexPath.row];
            
            [cell.containerView addGestureRecognizer:tapGesture];
            cell.labelDate.text = freeQuestionArray[indexPath.row][@"created"];
            cell.labelTime.text = [NSString stringWithFormat:@"%@",freeQuestionArray[indexPath.row][@"week_day"]];
            cell.lblQuestion.text = freeQuestionArray[indexPath.row][@"question"];
            
            
            if ([expandedRows containsIndex:indexPath.row]) {
                
                [UIView animateWithDuration:0.25 animations:^{
                    
                    [cell.arrow setTransform:CGAffineTransformMakeRotation(M_PI_2)];
                    
                }];
                
            } else {
              
                [UIView animateWithDuration:0.25 animations:^{
                    
                    [cell.arrow setTransform:CGAffineTransformMakeRotation(0)];
                    
                }];
            }
            
            if (![freeQuestionArray[indexPath.row][@"reply"] isEqualToString:@""]) {
                
                cell.lblAnswerHeader.text = [NSString stringWithFormat:@"You Replied | %@",freeQuestionArray[indexPath.row][@"reply_date"]];
                cell.lblAnswer.text = freeQuestionArray[indexPath.row][@"reply"];
                
                [cell.bttnReplyNow setHidden:YES];

            } else {
                
                cell.lblAnswerHeader.text = [NSString stringWithFormat:@"Reply Pending %@",freeQuestionArray[indexPath.row][@"reply_date"]];
                cell.lblAnswer.text = @"";
                
                [cell.bttnReplyNow setHidden:NO];

            }

            cell.lblStatus.text = freeQuestionArray[indexPath.row][@"status"];
            cell.lblAskedBy.text = freeQuestionArray[indexPath.row][@"asked_by"];
            [cell setSelectionStyle : UITableViewCellSelectionStyleNone];
            
            return cell;
        }
            
        case PaidQuestions : {
            
            MessageTimeSlotsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"questionCell"
                                                                                  forIndexPath:indexPath];
            cell.labelDate.text = paidQuestionsArray[indexPath.row][@"created"];
            cell.labelTime.text = [NSString stringWithFormat:@"%@",paidQuestionsArray[indexPath.row][@"time"]];
            [cell setSelectionStyle : UITableViewCellSelectionStyleNone];
            
            return cell;
        }
            
        case MissedCalls : {
            
            return nil;
        }
    }
    return [UITableViewCell new];
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:{
            
        } break;
            
        case PaidQuestions:{
            
            MessageTimeSlotsTableViewCell *myCell = (MessageTimeSlotsTableViewCell*)cell;
            
            if ([paidQuestionsArray[indexPath.row][@"reply"]isEqualToString:@""]) {
                
                [myCell.containerView setBackgroundColor:UIColorFromRGB(0x1E9EC3)];
                
            } else {
                
                [myCell.containerView setBackgroundColor:UIColorFromRGB(0xE96468)];

            }
        } break;
            
        case FreeQuestions:{
            
            MessageTimeSlotsTableViewCell *myCell = (MessageTimeSlotsTableViewCell*)cell;
            
            if ([freeQuestionArray[indexPath.row][@"reply"]isEqualToString:@""]) {
            
                [myCell.containerView setBackgroundColor:UIColorFromRGB(0x1E9EC3)];
                
            } else {
                
                [myCell.containerView setBackgroundColor:UIColorFromRGB(0xE96468)];
            }
        }
            break;
            
        case MissedCalls:
            
            break;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath : indexPath
                             animated : YES];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    MessageCenterDetailViewController *detailVc = [self.storyboard instantiateViewControllerWithIdentifier:@"messageCenterDetail"];
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:
        {
            switch (indexPath.row) {
                    
                case 0:{
                    
                    detailVc.title = @"New Consultation Requests";
                    
                    detailVc.timeSlotArray = appDelegate.messageCenterData[@"new_consultations"];
                    
                    [self.navigationController pushViewController:detailVc animated:YES];
                    
                }
                    break;
                    
                case 1:{
                    
                    detailVc.title = @"Confirmed Consultations";
                    
                    detailVc.timeSlotArray = appDelegate.messageCenterData[@"confirmed_consultations"];
                    
                    [self.navigationController pushViewController:detailVc animated:YES];
                }
                    break;
                    
                case 2:{
                    
                    detailVc.title = @"Previous Consultations";
                    
                    detailVc.timeSlotArray = appDelegate.messageCenterData[@"previous_consultations"];
                    
                    [self.navigationController pushViewController:detailVc animated:YES];
                }
                    break;
                    
                case 3:{
                    
                    detailVc.title = @"Cancelled Consultations";
                    
                    detailVc.timeSlotArray = appDelegate.messageCenterData[@"cancelled_consultations"];
                    
                    [self.navigationController pushViewController:detailVc animated:YES];
                    
                }
                    break;
                    
                case 4:{
                    
                    //gp_consultation
                    
                    detailVc.title = @"Next Available GP";
                    
                    detailVc.timeSlotArray = appDelegate.messageCenterData[@"gp_consultation"];
                    
                    [self.navigationController pushViewController:detailVc animated:YES];
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 1:{
            // Free Questions
            
//            MessageTimeSlotsTableViewCell *myCell = [tableView cellForRowAtIndexPath:indexPath];
//            
//            if ([expandedRows containsIndex:indexPath.row]) {
//                
//                [UIView animateWithDuration:0.25 animations:^{
//                    
//                    [myCell.arrow setTransform:CGAffineTransformMakeRotation(0)];
//
//                }];
//                [expandedRows removeIndex:indexPath.row];
//                
//            } else {
//               
//                [UIView animateWithDuration:0.25 animations:^{
//                    
//                    [myCell.arrow setTransform:CGAffineTransformMakeRotation(M_PI_2)];
//                    
//                }];
//                [expandedRows addIndex:indexPath.row];
//            }
//            [tableView reloadRowsAtIndexPaths:@[indexPath]
//                             withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }  break;
        default:
            break;
    }
    
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width, 30)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 200, 20)];
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:
            
            headerLabel.text = @"Online Consultations";
            
            [headerView addSubview:headerLabel];
            
            return headerView;
            
        case FreeQuestions:
            
            headerLabel.text = @"Free Questions";
            
            [headerView addSubview:headerLabel];
            
            return headerView;
            
        case PaidQuestions:
            
            headerLabel.text = @"Paid Questions";
            
            [headerView addSubview:headerLabel];
            
            return headerView;
            
        case MissedCalls:
            
            headerLabel.text = @"Missed Calls";
            
            [headerView addSubview:headerLabel];
            
            return headerView;
            
        default:
            return nil;
    }
}

// web_doctor_online_consultations : For Section 0 (Only)

#pragma mark - Calling API's

-(void)getMessageCenterDataWithAPICallMode:(APICallMode)mode {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (mode == Foreground) {
      
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    }
    
    if (appDelegate.currentInternetStatus) {
        
        NSString *urlAsString = [NSString stringWithFormat:@"%@web_doctor_message_centre_consultations",BASE_URL];
        
        NSLog(@"MessageCenter API ----> %@",urlAsString);
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *parameters = @{@"doctor_id" : [PersistencyManager getDoctorId] ,
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        
        [manager.requestSerializer setTimeoutInterval:10];
        
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 if (mode == Foreground) {
                     
                     [SVProgressHUD dismiss];
                     
                 } else {
                     
                     [refreshControl endRefreshing];
                 }
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     appDelegate.messageCenterData = responseObject;
                     
                     [_tableView reloadData];
                     
                     NSLog(@"MessageCenter Get Consultations API Response ----> %@",appDelegate.messageCenterData);
                     
                 } else {
                    
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 NSLog(@" MessageCenter API --- Error |||_______---->>>%@" , [error localizedDescription]);
                 
                 [SVProgressHUD dismiss];
                 
                 [CustomAlertController showAlertOnController:self
                                                withAlertType:Simple
                                                andAttributes:@{
                                                                @"title" : @"Error",
                                                                @"message" :@""
                                                                }];
             }];
    } else {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Network
                                       andAttributes : nil];
    }
}
-(void)getMissedCallsDataWithAPICallMode:(APICallMode)mode {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (mode == Foreground) {
     
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    }
    
    if (appDelegate.currentInternetStatus) {
        
        NSString *urlAsString = [NSString stringWithFormat:@"https://logintohealth.com/demo/webservices/appdoctorOther/missed_calls"];
        
        NSLog(@"MessageCenter API ----> %@",urlAsString);
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *parameters = @{@"doctor_id" : [PersistencyManager getDoctorId] ,
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        [manager.requestSerializer setTimeoutInterval:10];
        
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 if (mode == Foreground) {
                     
                     [SVProgressHUD dismiss];
                     
                 } else {
                     
                     [refreshControl endRefreshing];
                 }
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     appDelegate.missedCallsData = responseObject;
                     
                     NSLog(@"MessageCenter Missed Calls API Response ----> %@",appDelegate.missedCallsData);
                     
                    
                 } else {
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 NSLog(@" MessageCenter API --- Error |||_______---->>>%@" , [error localizedDescription]);
                 
                 if (mode == Foreground) {
                     
                     [SVProgressHUD dismiss];
                     
                 } else {
                     
                     [refreshControl endRefreshing];
                 }

                 [CustomAlertController showAlertOnController:self
                                                withAlertType:Simple
                                                andAttributes:@{
                                                                @"title" : @"Error",
                                                                @"message" :@""
                                                                }];
             }];
    } else {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Network
                                       andAttributes : nil];
    }
}
-(void)getPaidQuestionDataWithAPICallMode:(APICallMode)mode {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (mode == Foreground) {
      
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];

    }
    if (appDelegate.currentInternetStatus) {
        
        NSString *urlAsString = [NSString stringWithFormat:@"%@get_paid_questions",BASE_URL];
        
        NSLog(@"MessageCenter API ----> %@",urlAsString);
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *parameters = @{@"doctor_id" : [PersistencyManager getDoctorId]
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        
        [manager.requestSerializer setTimeoutInterval:10];
        
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 if (mode == Foreground) {
                     
                     [SVProgressHUD dismiss];
                     
                 } else {
                     
                     [refreshControl endRefreshing];
                 }
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     paidQuestionsArray = responseObject[@"my_questions"];
                     
                     NSLog(@"MessageCenter Paid Question API Response ----> %@",paidQuestionsArray);
                     
                     [_tableView reloadData];
                  
                     
                     
                 } else {
                     
                     // When The Server Returns NULL
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 NSLog(@" MessageCenter API --- Error |||_______---->>>%@" , [error localizedDescription]);
                 
                 if (mode == Foreground) {
                     
                     [SVProgressHUD dismiss];
                 } else {
                     
                     [refreshControl endRefreshing];
                 }
                 
                 [CustomAlertController showAlertOnController:self
                                                withAlertType:Simple
                                                andAttributes:@{
                                                                @"title" : @"Error",
                                                                @"message" :@""
                                                                }];
             }];
    } else {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Network
                                       andAttributes : nil];
    }
}

-(void)getFreeQuestionDataWithAPICallMode:(APICallMode)mode  {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (mode == Foreground) {
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    }
    
    if (appDelegate.currentInternetStatus) {
        
        NSString *urlAsString = [NSString stringWithFormat:@"%@get_free_questions",BASE_URL];
        
        NSLog(@"MessageCenter API ----> %@",urlAsString);
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *parameters = @{@"doctor_id" : [PersistencyManager getDoctorId] ,
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        
        [manager.requestSerializer setTimeoutInterval:10];
        
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 if (mode == Foreground) {
                     
                     [SVProgressHUD dismiss];
                     
                 } else {
                     
                     [refreshControl endRefreshing];
                 }
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     freeQuestionArray = responseObject[@"open_questions"];
                     
                     NSLog(@"Free Question Array----------------->\n%@",freeQuestionArray);
                     
                     [_tableView reloadData];
                     
                 } else {
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 if (mode == Foreground) {
                     
                     [SVProgressHUD dismiss];
                     
                 } else {
                     
                     [refreshControl endRefreshing];
                 }
                 
                 [CustomAlertController showAlertOnController:self
                                                withAlertType:Simple
                                                andAttributes:@{
                                                                @"title" : @"Error",
                                                                @"message" :@""
                                                                }];
             }];
    } else {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Network
                                       andAttributes : nil];
    }
}
- (IBAction)segmentedControlPressed:(id)sender {
    
    [_tableView reloadData];
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:{
            
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            
            if (!delegate.messageCenterData) {
                
                [self getMessageCenterDataWithAPICallMode:Foreground];
            }
        } break;
            
        case PaidQuestions:
            
            if (paidQuestionsArray.count == 0) {
                
                [self getPaidQuestionDataWithAPICallMode:Foreground];
            }
            break;
            
        case FreeQuestions:
            
            if (freeQuestionArray.count == 0) {
                
                [self getFreeQuestionDataWithAPICallMode:Foreground];
            }
            break;
            
        case MissedCalls:
            [self getMissedCallsDataWithAPICallMode:Foreground];
            break;
    }
}

#pragma mark - Pull To Refresh

-(void)getData {
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:
            [self getMessageCenterDataWithAPICallMode:Background];
         break;
        case PaidQuestions:
            [self getPaidQuestionDataWithAPICallMode:Background];
            break;
        case FreeQuestions:
            [self getFreeQuestionDataWithAPICallMode:Background];
            break;
        case MissedCalls:
            [self getMissedCallsDataWithAPICallMode:Background];
            break;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    switch (_segmentedControl.selectedSegmentIndex) {
            
        case OnlineConsultations:
            break;
        case PaidQuestions:
            return 56;
            
        case FreeQuestions:{
            
            CGFloat questionHeight = [HelperClass getStringWidthWithString:freeQuestionArray[indexPath.row][@"question"]
                                                                   andFont:[UIFont fontWithName:@"Helvetica-Bold"
                                                                                           size:15]];
            
            CGFloat answerHeight  = [HelperClass getStringWidthWithString:freeQuestionArray[indexPath.row][@"reply"]
                                                                  andFont:[UIFont fontWithName:@"Helvetica"
                                                                                          size:14]];
            
            CGFloat askedByHeight = [HelperClass getStringWidthWithString:freeQuestionArray[indexPath.row][@"asked_by"]
                                                                  andFont:[UIFont fontWithName:@"Helvetica"
                                                                                          size:15]];
            
            CGFloat statusHeight = [HelperClass getStringWidthWithString:freeQuestionArray[indexPath.row][@"status"]
                                                                 andFont:[UIFont fontWithName:@"Helvetica"
                                                                                         size:14]];
            
            CGFloat totalHeight = questionHeight + answerHeight + askedByHeight + statusHeight;
            
            if ([expandedRows containsIndex:indexPath.row]) {
                
                if ([freeQuestionArray[indexPath.row][@"reply"] isEqualToString:@""]) {
                    
                    return totalHeight + 170;
                }
                
                return totalHeight + 160;
                
            } else {
                
                return 58;
            }
        }
        case MissedCalls:{
            
        }
        break;
    }
    return 56;
}

#pragma mark - Custom Selector

-(void)freeQuestionContainerTouched:(UITapGestureRecognizer*)sender {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender.view tag] inSection:0];
    
    MessageTimeSlotsTableViewCell *myCell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([expandedRows containsIndex:indexPath.row]) {
        
        [UIView animateWithDuration:0.25 animations:^{
            
            [myCell.arrow setTransform:CGAffineTransformMakeRotation(0)];
            
        }];
        [expandedRows removeIndex:indexPath.row];
        
    } else {
        
        [UIView animateWithDuration:0.25 animations:^{
            
            [myCell.arrow setTransform:CGAffineTransformMakeRotation(M_PI_2)];
            
        }];
        [expandedRows addIndex:indexPath.row];
    }
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}
-(void)replyNowTouched:(UIButton*)sender {
    
    ReplyQuestionViewController *replyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"replyPopUp"];
    replyViewController.providesPresentationContextTransitionStyle = YES;
    
    replyViewController.questionData = freeQuestionArray[[sender tag]];
    
    replyViewController.definesPresentationContext = YES;
    [replyViewController setModalPresentationStyle : UIModalPresentationOverCurrentContext];
    
    [self presentViewController : replyViewController
                       animated : YES
                     completion : nil];
}
@end