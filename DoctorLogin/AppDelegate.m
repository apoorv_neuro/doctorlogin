//
//  AppDelegate.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 28/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "AppDelegate.h"
#import <AFNetworking.h>
#import "DashboardViewController.h"
#import "MenuItemsTVC.h"
#import "DoctorLogin-Swift.h"
#import "ViewController.h"
#import "PersistencyManager.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

#pragma mark - Application Lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Start Network Change Notifier
    
    [self checkInternetConnection];
    [self checkNetworkStatus:nil];
    
    // Preload Data For Application
    
    [self getAllData];
    
    if ([PersistencyManager getDoctorId]) {
        
        [self showDashboard];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(logoutNotificationReceived)
                                                 name:@"logout"
                                               object:nil];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
}
- (void)applicationWillTerminate:(UIApplication *)application {
}
#pragma mark - Internet Reachability Check

-(void)checkInternetConnection {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkNetworkStatus:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    
    self.hostReachability = [Reachability reachabilityWithHostname:BASE_URL];
    [self.hostReachability startNotifier];
}

#pragma mark - Network Change Notifier

-(void)checkNetworkStatus:(NSNotification *)notice {
    
    NetworkStatus internetStatus = [self.internetReachability currentReachabilityStatus];
    
    switch (internetStatus)
    {
        case NotReachable:{
            _currentInternetStatus = NO;
            break;
        }
        case ReachableViaWiFi:{
            
            _currentInternetStatus = YES;
            break;
        }
        case ReachableViaWWAN:{
            
            _currentInternetStatus = YES;
            break;
        }
    }
    NetworkStatus hostStatus = [self.hostReachability currentReachabilityStatus];
    
    switch (hostStatus){
            
        case NotReachable:{
            
            _currentHostStatus = NO;
            break;
        }
        case ReachableViaWiFi:{
            
            _currentHostStatus = YES;
            break;
        }
        case ReachableViaWWAN:{
            
            _currentHostStatus = YES;
            break;
        }
    }
}
#pragma mark - Preloading Data

-(void)getAllData {
    
    //getalldata (list of state , city , specialisations)
    
    if (self.currentInternetStatus) {
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSString *urlAsString = [NSString stringWithFormat:@"%@getalldata",BASE_URL];
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        [manager GET : urlAsString
          parameters : nil
            progress : nil
              success: ^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                  
                  NSLog(@"Get All Data --- Response---->%@",responseObject);
                  
                  if (responseObject != nil && responseObject != NULL) {
                      
                      // Preloading Cities
                      
                      if ([responseObject[@"city"] isKindOfClass:[NSArray class]]) {
                          
                          _cityArray = responseObject[@"city"];
                      }
                      
                      // Preloading States
                      
                      if ([responseObject[@"state"] isKindOfClass:[NSArray class]]) {
                          
                          _stateArray = responseObject[@"state"];
                      }
                      
                      // Preloading Specialisations
                      
                      if ([responseObject[@"specialisations"] isKindOfClass:[NSArray class]]) {
                          
                          _specialisationArray = responseObject[@"specialisations"];
                      }
                      
                  } else {
                      
                      // When The Server Returns NULL
                  }
              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  
                  NSLog(@"Get All Data --- Error |||_______---->>>%@",[error localizedDescription]);
              }];
    }
}

#pragma mark - Show Dashboard

-(void)showDashboard {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DashboardViewController *dashboard = [storyboard instantiateViewControllerWithIdentifier:@"dashboardScene"];
    
    MenuItemsTVC * sideMenu = [MenuItemsTVC sharedInstance];
    
    sideMenu.menuArray = [NSArray arrayWithObjects:@"Dashboard",
                          @"Message Centre"
                          ,@"Calendar"
                          ,@"Patients"
                          ,@"My Earnings"
                          ,@"My Account"
                          ,@"My Feedback",
                          @"My Setting",
                          @"Change Password",
                          @"Logout"
                          , nil];
    
    ENSideMenuNavigationController *naviController = [[ENSideMenuNavigationController alloc] initWithMenuViewController:sideMenu
                                                      contentViewController:dashboard];
    
    [naviController.sideMenu setMenuWidth:[[UIScreen mainScreen] bounds].size.width - 120];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setRootViewController:naviController];
    [self.window makeKeyAndVisible];
}
#pragma mark - Logout Notification

-(void)logoutNotificationReceived {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *navController = [storyboard instantiateViewControllerWithIdentifier:@"initialController"];
    [self.window setRootViewController:navController];
    [self.window makeKeyAndVisible];
}
@end