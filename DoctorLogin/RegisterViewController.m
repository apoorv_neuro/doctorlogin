//
//  RegisterViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 29/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"
#import "CustomAlertController.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import "OTPViewController.h"
#import <TTTAttributedLabel.h>
#import "LoginViewController.h"

@interface RegisterViewController ()< UIPickerViewDelegate ,UIPickerViewDataSource ,UITextFieldDelegate,TTTAttributedLabelDelegate> {
    
    NSArray *genderArray;
    UIPickerView *pickerView;
}
@property (weak, nonatomic) IBOutlet UILabel *lblSignIn;

// For Validations On Text Field

@property (strong , nonatomic) ValidatedTextField *validation;
@property (weak, nonatomic) IBOutlet UITextField *txtFldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtFldLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtFldGender;
@property (weak, nonatomic) IBOutlet UITextField *txtFlfMobileNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtFldCity;
@property (weak, nonatomic) IBOutlet UITextField *txtFldState;
@property (weak, nonatomic) IBOutlet UITextField *txtFldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtFldConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmailId;
@property (weak, nonatomic) IBOutlet UITextField *txtFldSpecialisation;
@property (weak, nonatomic) IBOutlet UITextField *txtFldRegistrationNumber;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTop;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    NSString *labelText = @"If you have already registered , Please Sign In";
    _lblTop.text = labelText;
    NSRange r = [labelText rangeOfString:@"Sign In"];
    [_lblTop addLinkToURL:[NSURL URLWithString:@"action://show-help"] withRange:r];
    
    _lblTop.delegate = self;
    
    // Initialising Local Array
    
    genderArray = @[  @"Male"
                     ,@"Female"];
    
    // Initialising Picker
    
    pickerView = [[UIPickerView alloc]init];
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    // Setting Picker On Text Fields
    
    _txtFldGender.inputView = pickerView;
    _txtFldGender.delegate = self;
    
    _txtFldCity.inputView = pickerView;
    _txtFldCity.delegate = self;
    
    _txtFldState.inputView = pickerView;
    _txtFldState.delegate = self;
    
    _txtFldSpecialisation.inputView = pickerView;
    _txtFldSpecialisation.delegate = self;
    
    // Setting "down_arrow" Image On TextFields
    
    [self setImageOnTextField:_txtFldGender];
    [self setImageOnTextField:_txtFldCity];
    [self setImageOnTextField:_txtFldState];
    [self setImageOnTextField:_txtFldSpecialisation];
    
    // Setting Validations
    
    _validation = [[ValidatedTextField alloc]init];
    
    _txtFldFirstName.delegate = _validation;
    _txtFldLastName.delegate = _validation;
    _txtFldEmailId.delegate = _validation;
    _txtFlfMobileNumber.delegate = _validation;
    
    
    if (_loginTag) {
        
        [self autoFillData];
    }
    
}
- (IBAction)buttonOpenPatientAppTouched:(id)sender {
    
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"patientapp://"]];
    
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        
        [[UIApplication sharedApplication] openURL: whatsappURL];
        
    } else {
        
        [CustomAlertController showAlertOnController:self
                                       withAlertType:Simple
                                       andAttributes:@{
                                                       @"title" : @"App Not Found",
                                                       @"message" : @"Kindly Install The App First"
                                                       }];
    }
}

#pragma mark - Picker View DataSource

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    // Gender
    if ([_txtFldGender isFirstResponder]) {
        
        return [genderArray count];
    }
    
    // City
    if ([_txtFldCity isFirstResponder]) {
        
        return appDelegate.cityArray.count;
    }
    
    // State
    if ([_txtFldState isFirstResponder]) {
        
        return appDelegate.stateArray.count;
    }
    
    // Specialisaion
    if ([_txtFldSpecialisation isFirstResponder]) {
        
        return appDelegate.specialisationArray.count;
    }
    return 0;
}
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    // Gender
    if ([_txtFldGender isFirstResponder]) {
        
        return genderArray[row];
    }
    // City
    if ([_txtFldCity isFirstResponder]) {
        
        return appDelegate.cityArray[row][@"city_name"];
    }
    
    // State
    if ([_txtFldState isFirstResponder]) {
        
        return appDelegate.stateArray[row][@"state_name"];
    }
    
    // Specialisaion
    if ([_txtFldSpecialisation isFirstResponder]) {
        
        return appDelegate.specialisationArray[row][@"specialisation"];
    }
    
    return @"";
}
#pragma mark - Picker View Delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    // Gender
    if ([_txtFldGender isFirstResponder]) {
        
        _txtFldGender.text = genderArray[row];
    }
    
    // City
    if ([_txtFldCity isFirstResponder]) {
        
        _txtFldCity.text = appDelegate.cityArray[row][@"city_name"];
    }
    
    // State
    if ([_txtFldState isFirstResponder]) {
        
        _txtFldState.text = appDelegate.stateArray[row][@"state_name"];
    }
    
    // Specialisaion
    if ([_txtFldSpecialisation isFirstResponder]) {
        
        _txtFldSpecialisation.text = appDelegate.specialisationArray[row][@"specialisation"];
    }
}
#pragma mark - Text Field Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    NSInteger selectedIndex;
    
    [pickerView reloadAllComponents];
    
    // Gender
    
    if ([_txtFldGender isFirstResponder]) {
        
        if ([_txtFldGender.text isEqualToString:@""] || _txtFldGender.text == nil) {
            
            selectedIndex = 0;
            
            _txtFldGender.text = genderArray[0];
            
        } else {
            
            selectedIndex = [HelperClass getPickerIndexWithArray:genderArray
                                                       andString:_txtFldGender.text];
        }
        [pickerView selectRow:selectedIndex
                  inComponent:0
                     animated:YES];
    }
    
    // City
    
    if ([_txtFldCity isFirstResponder]) {
        
        if ([_txtFldCity.text isEqualToString:@""] || _txtFldCity.text == nil) {
            
            selectedIndex = 0;
            
            _txtFldCity.text = appDelegate.cityArray[0][@"city_name"];
            
        } else {
            
            selectedIndex = [HelperClass getPickerIndexWithArray:appDelegate.cityArray associatedKey:@"city_name" andString:_txtFldCity.text];
        }
        [pickerView selectRow:selectedIndex
                  inComponent:0
                     animated:YES];
    }
    
    // State
    
    if ([_txtFldState isFirstResponder]) {
        
        if ([_txtFldState.text isEqualToString:@""] || _txtFldState.text == nil) {
            
            selectedIndex = 0;
            
            _txtFldState.text = appDelegate.stateArray[0][@"state_name"];
            
        } else {
            
            selectedIndex = [HelperClass getPickerIndexWithArray:appDelegate.stateArray associatedKey:@"state_name" andString:_txtFldState.text];
        }
        [pickerView selectRow:selectedIndex
                  inComponent:0
                     animated:YES];
    }
    
    // Specialisation
    
    if ([_txtFldSpecialisation isFirstResponder]) {
        
        if ([_txtFldSpecialisation.text isEqualToString:@""] || _txtFldSpecialisation.text == nil) {
            
            selectedIndex = 0;
            
            _txtFldSpecialisation.text = appDelegate.specialisationArray[0][@"specialisation"];
            
        } else {
            
            selectedIndex = [HelperClass getPickerIndexWithArray:appDelegate.specialisationArray
                                                   associatedKey:@"specialisation"
                                                       andString:_txtFldSpecialisation.text];
        }
        [pickerView selectRow:selectedIndex
                  inComponent:0
                     animated:YES];
    }
}

#pragma mark - Custom Methods

-(void)setImageOnTextField:(UITextField*)txtField {
    
    UIImageView *downArrow = [[UIImageView alloc]initWithFrame:CGRectMake(  0.0
                                                                          , 0.0
                                                                          , 30
                                                                          , 30)];
    [downArrow setImage:[UIImage imageNamed:@"down_arrow"]];
    [downArrow setContentMode:UIViewContentModeCenter];
    
    [txtField setRightViewMode:UITextFieldViewModeAlways];
    txtField.rightView = downArrow;
}

#pragma mark - Register

- (IBAction)continueButtonTouched:(id)sender {
    
    // Case 1 :
    
    // Empty TextField Check
    
    if ([_txtFldFirstName.text isEqualToString:@""]
        ||
        [_txtFldLastName.text isEqualToString:@""]
        ||
        [_txtFldEmailId.text isEqualToString:@""]
        ||
        [_txtFlfMobileNumber.text isEqualToString:@""]
        ||
        [_txtFldGender.text isEqualToString:@""]
        ||
        [_txtFldPassword.text isEqualToString:@""]
        ||
        [_txtFldConfirmPassword.text isEqualToString:@""]
        ||
        [_txtFldRegistrationNumber.text isEqualToString:@""]
        ||
        [_txtFldSpecialisation.text isEqualToString:@""]
        ||
        [_txtFldState.text isEqualToString:@""]
        ||
        [_txtFldCity.text isEqualToString:@""]
        ) {
        
        [ CustomAlertController showAlertOnController:self
                                        withAlertType:Simple
                                        andAttributes:@{
                                                        @"title" : @"Incomplete Details !",
                                                        @"message" : @"Please Complete Your Details First"
                                                        }];
    } else {
        
        // Case 2
        
        // Password And Cnf Password Should Match
        
        if ([_txtFldPassword.text isEqualToString:_txtFldConfirmPassword.text]) {
            
            // Case 3
            
            // Validate Email Id
            
            if ([HelperClass validateEmailAddress:_txtFldEmailId.text]) {
                
                // Case 4
                
                // Validate Mobile Number
                
                if ([HelperClass validateMobileNumber:_txtFlfMobileNumber.text]) {
                    
                    // Validations Complete
                    
                    // Make Network Request
                    
                    [self makeNetworkRequest];
                    
                } else {
                    
                    [CustomAlertController showAlertOnController:self
                                                   withAlertType:Simple
                                                   andAttributes:@{
                                                                   @"title" : @"Invalid Mobile Number",
                                                                   @"message" : @"Enter A Valid Mobile Number"
                                                                   }];
                }
            } else {
                
                [CustomAlertController showAlertOnController:self
                                               withAlertType:Simple
                                               andAttributes:@{
                                                               @"title" : @"Invalid Email",
                                                               @"message" : @"Enter A Valid Email Address"
                                                               }];
            }
        } else {
            
            [CustomAlertController showAlertOnController:self
                                           withAlertType:Simple
                                           andAttributes:@{
                                                           @"title" : @"",
                                                           @"message" : @"Passwords Do Not Match"
                                                           }];
        }
    }
}

#pragma mark - Utility Methods

// Convert fULL Gender To The One That Is Required By The API

-(NSString*)selectGender:(NSString*)selectedGender {
    
    if ([selectedGender isEqualToString:@"Male"]) {
        
        return @"M";
        
    } else {
        
        return @"F";
    }
}
// Get City Id

-(NSString*)getCityId:(NSString*)cityName {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    NSInteger index = [HelperClass getPickerIndexWithArray:appDelegate.cityArray
                                             associatedKey:@"city_name"
                                                 andString:cityName];
    
    return appDelegate.cityArray[index][@"city_id"];
}

// Get State Id

-(NSString*)getStateId:(NSString*)stateName {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    NSInteger index = [HelperClass getPickerIndexWithArray:appDelegate.stateArray
                                             associatedKey:@"state_name"
                                                 andString:stateName];
    
    return appDelegate.stateArray[index][@"state_id"];
}

// Get Specialisation Id

-(NSString*)getSpecialisationId:(NSString*)specialisationName {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    NSInteger index = [HelperClass getPickerIndexWithArray:appDelegate.specialisationArray
                                             associatedKey:@"specialisation"
                                                 andString:specialisationName];
    
    return appDelegate.specialisationArray[index][@"specialisation_id"];
    
}

#pragma mark - Send Registration Details To API

- (void)makeNetworkRequest {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentInternetStatus) {
        
        [SVProgressHUD showWithStatus:@"Please Wait.." maskType:SVProgressHUDMaskTypeGradient];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        
        //first_name * , last_name * , email_id * , password * , phone * , registration_no * , specialisation_id * , state * , gender , city *
        NSString *urlAsString = [NSString stringWithFormat:@"%@signup",BASE_URL];
        
        NSDictionary *parameters = @{@"first_name":_txtFldFirstName.text ,
                                     @"last_name":_txtFldLastName.text,
                                     @"email_id":_txtFldEmailId.text,
                                     @"password":_txtFldPassword.text,
                                     @"phone":_txtFlfMobileNumber.text,
                                     @"registration_no":_txtFldRegistrationNumber.text,
                                     @"specialisation_id":[self getSpecialisationId:_txtFldSpecialisation.text],
                                     @"state":[self getStateId:_txtFldState.text],
                                     @"gender":[self selectGender:_txtFldGender.text],
                                     @"city":[self getCityId:_txtFldCity.text]
                                     };
        NSLog(@"Register Parameters------>>>>> %@",parameters);
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 [SVProgressHUD dismiss];
                 
                 NSLog(@" Register API --- Response---->%@",responseObject);
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     if ([responseObject[@"message"] isKindOfClass:[NSDictionary class]]) {
                         
                         // Case   : Success
                         OTPViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"otpScene"];
                         
                         vc.mobileNumber = _txtFlfMobileNumber.text;
                         vc.doctorId = responseObject[@"message"][@"doctor_id"];
                         
                         [self presentViewController:vc animated:YES completion:nil];
                         
 
                     } else {
                         
                         if ([responseObject[@"message"]isKindOfClass:[NSString class]]) {
                             
                             [CustomAlertController showAlertOnController:self
                                                            withAlertType:Simple
                                                            andAttributes:@{
                                                                            @"title" : @"",
                                                                            @"message" : responseObject[@"message"]
                                                                            }];
                         }
                     }
                 } else {
                     
                     // When The Server Returns NULL
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 
                 [SVProgressHUD dismiss];
                 
                 NSLog(@"Register API --- Error |||_______---->>>%@",[error localizedDescription]);
             }];
    } else {
        
        [CustomAlertController showAlertOnController:self
                                       withAlertType:Network
                                       andAttributes:nil];
    }
}

#pragma mark -  Autofill Using Info From Facebook Or Google

-(void)autoFillData {
    
    if (_loginTag==0) {
        
//        if ([self.responseData valueForKey:@"birthday"]) {
//            
//            self.txtfld.text = [self.responseData valueForKey:@"birthday"];
//        }
        if ([self.responseData valueForKey:@"gender"]) {
            
            self.txtFldGender.text = [[self.responseData valueForKey:@"gender"] capitalizedString];
        }
        if ([self.responseData valueForKey:@"name"]) {
            
            if ([[self.responseData valueForKey:@"name"] isKindOfClass:[NSDictionary class]]) {
                
                self.txtFldFirstName.text =[[self.responseData valueForKey:@"name"]valueForKey:@"givenName"];
                
                self.txtFldFirstName.text = [(NSDictionary*)[self.responseData valueForKey:@"name"] valueForKey:@"familyName"];
            }
        }
        if ([[self.responseData valueForKey:@"emails"]objectAtIndex:0]) {
            
            self.txtFldEmailId.text = [(NSString*)[[self.responseData valueForKey:@"emails"]objectAtIndex:0] valueForKey:@"value"];
        }
        
    } else {
        
        if ([self.responseData valueForKey:@"gender"]){
            self.txtFldGender.text = [[self.responseData valueForKey:@"gender"] capitalizedString];
        }
        if ([self.responseData valueForKey:@"first_name"]) {
            
            self.txtFldFirstName.text =[self.responseData valueForKey:@"first_name"];
        }
        if ([self.responseData valueForKey:@"last_name"]) {
            
            self.txtFldLastName.text =[self.responseData valueForKey:@"last_name"];
        }
        
        if ([self.responseData valueForKey:@"email"]) {
            
            self.txtFldEmailId.text = [self.responseData valueForKey:@"email"];
        }
    }
}
#pragma mark - TTAttributedString Delegate
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    
    if ([[url scheme] hasPrefix:@"action"]) {
        if ([[url host] hasPrefix:@"show-help"]) {
            /* load help screen */
            // Send back to login view controller
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }
    } else {
        /* deal with http links here */
    }
}
@end
