//
//  DashboardCollectionViewCell.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 10/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "DashboardCollectionViewCell.h"

@implementation DashboardCollectionViewCell

-(void)awakeFromNib {

    [super awakeFromNib];
    self.contentView.frame = self.bounds;
    self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}
- (void)layoutSubviews {
    
    [super layoutSubviews];

    _activityIndicator.hidesWhenStopped = YES;
}
@end
