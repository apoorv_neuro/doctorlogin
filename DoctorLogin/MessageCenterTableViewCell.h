//
//  MessageCenterTableViewCell.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 23/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCenterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

// Header Cell For PatientDetail

@property (weak, nonatomic) IBOutlet UILabel *headerPatientName;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UILabel *patientId;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblMedium;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

@end
