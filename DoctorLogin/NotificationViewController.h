//
//  NotificationViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 04/06/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController <UIViewControllerTransitioningDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewNotification;
@end
