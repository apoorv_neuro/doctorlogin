//
//  PatientDetailViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 14/06/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "PatientDetailViewController.h"
#import <AFNetworking.h>
#import "PersistencyManager.h"
#import <SVProgressHUD.h>
#import "CustomAlertController.h"
#import "AppDelegate.h"
#import "MessageCenterTableViewCell.h"
#import "PatientDetailTableViewCell.h"
#import <UIImageView+AFNetworking.h>


@interface PatientDetailViewController () < UITableViewDataSource , UITableViewDelegate >{
    
    NSMutableIndexSet *expandedSections;
    NSDictionary *patientDetails;
}
@property (weak , nonatomic) IBOutlet UITableView *tableView;
@end

@implementation PatientDetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
        
    _tableView.rowHeight = UITableViewAutomaticDimension;
    [_tableView reloadData];
    
    
    expandedSections = [NSMutableIndexSet new];
    
    [self getPatientData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
   
    
    return patientDetails!=nil?[patientDetails[@"Section_Headers"]count]+1:0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row > 0) {
        
        if (_contentMode == OnlineConsultation & indexPath.section == 3) {
            
            PatientDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionCell" forIndexPath:indexPath];
            
            return cell;
            
        } else {
            
            PatientDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"personalDetailCell" forIndexPath:indexPath];
            
            if (_contentMode == OnlineConsultation) {
                
                cell.lbl_0.text = patientDetails[@"Sub_Headers"][indexPath.section-1][0];
                cell.lbl_1.text = patientDetails[@"Sub_Headers"][indexPath.section-1][1];
                cell.lbl_2.text = patientDetails[@"Sub_Headers"][indexPath.section-1][2];
                cell.lbl_3.text = patientDetails[@"Sub_Headers"][indexPath.section-1][3];
                cell.lbl_4.text = patientDetails[@"Sub_Headers"][indexPath.section-1][4];
                
                if (indexPath.section == 1) {
                    
                    cell.lbl_11.text = patientDetails[@"Phone"];
                    cell.lbl_12.text = patientDetails[@"Email_Id"];
                    cell.lbl_13.text = patientDetails[@"Blood_Group"];
                    cell.lbl_14.text = patientDetails[@"DOB"];
                    cell.lbl_15.text = patientDetails[@"Address"];
                    
                } else {
                    
                    cell.lbl_11.text = patientDetails[@"Consultation_Date"];
//                    cell.lbl_12.text = patientDetails[@"Email_Id"];
                    cell.lbl_13.text = patientDetails[@"ReasonAppt"];
                    cell.lbl_14.text = patientDetails[@"Symptoms"];
                    cell.lbl_15.text = patientDetails[@"Allergy"];

                }
            }
            
            if (_contentMode == PaidQuestion) {
                
                if (indexPath.section == 1) {
                    
                    cell.lbl_11.text = patientDetails[@"Phone"];
                    cell.lbl_12.text = patientDetails[@"Email_Id"];
                    cell.lbl_13.text = patientDetails[@"Blood_Group"];
                    cell.lbl_14.text = patientDetails[@"DOB"];
                    cell.lbl_15.text = patientDetails[@"Address"];
                    
                }
            }
            return cell;
            
        }
    } else {
        
        if (indexPath.section == 0) {
            
            MessageCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath:indexPath];
            
            cell.headerPatientName.text = patientDetails[@"Patient_name"];
            cell.patientId.text = [NSString stringWithFormat:@"ID :%@",patientDetails[@"Patient_Id"]];
            cell.lblGender.text = patientDetails[@"Gender"];
            
            [cell.headerImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://logintohealth.com/demo/assets/uploads/patients/%@",patientDetails[@"Profile_image"]]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
            
            if (_contentMode == OnlineConsultation) {
                
                cell.lblMedium.text = patientDetails[@"Medium"];
            }
            return cell;
            
        } else {
            
            MessageCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sectionCell" forIndexPath:indexPath];
            
            cell.labelTitle.text = patientDetails[@"Section_Headers"][indexPath.section-1];
            
            return cell;
        }
    }
    return nil;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([expandedSections containsIndex:section]) {
        
        return 2;
    }
    return patientDetails != nil ? 1 : 0;
}
-(void)getPatientData {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    
    if (appDelegate.currentInternetStatus) {
        
        NSString *urlAsString;
        
        NSLog(@"MessageCenter API ----> %@",urlAsString);
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *parameters =[NSDictionary new];
        
        switch (_contentMode) {
                
            case OnlineConsultation:{
                
                urlAsString = [NSString stringWithFormat:@"%@web_doctor_message_centre_previous_view_details",BASE_URL];
                
                parameters = @{@"consultation_id" : appDelegate.dashboardData[@"new_consultation"][0][@"consultation_id"],
                               };
            }
                break;
                
            case PaidQuestion:{
                
                urlAsString = [NSString stringWithFormat:@"%@view_data",BASE_URL];
                
                parameters = @{@"question_id" : appDelegate.dashboardData[@"next_paid_question"][0][@"question_id"],
                               @"doctor_id"   : [PersistencyManager getDoctorId],
                               @"option"      : @"my-questions"
                               };
            }
                break;
                
            case NextConsultation:{
                
            }
                break;
            case NextPediatricionCall:{
                
            }
                break;
        }
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        
        [manager.requestSerializer setTimeoutInterval:10];
        
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     NSLog(@"API Response ----> %@",responseObject);
                     
                     [self prepareData:responseObject];
                     
                     [SVProgressHUD dismiss];
                     
                 } else {
                     
                     [SVProgressHUD dismiss];
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 NSLog(@" web_doctor_message_centre_previous_view_details API --- Error |||_______---->>>%@" , [error localizedDescription]);
                 
                 [SVProgressHUD dismiss];
                 
                 [CustomAlertController showAlertOnController:self
                                                withAlertType:Simple
                                                andAttributes:@{
                                                                @"title" : @"Error",
                                                                @"message" :@"Server Did Not Respond"
                                                                }];
             }];
    } else {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Network
                                       andAttributes : nil];
    }
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        return 100;
    }
    if (indexPath.section > 0) {
        
        return 62;
    }
    return 200;
}
-(void)prepareData:(id)responseObject {
    
    switch (_contentMode) {
            
        case OnlineConsultation:{
            
            patientDetails = @{@"Patient_name"  : responseObject[@"detail_data"][@"patient_name"]
                               ,@"Address":responseObject[@"detail_data"][@"address"],
                               @"Profile_image" : responseObject[@"detail_data"][@"profile_image"],
                               @"Symptoms"      : responseObject[@"detail_data"][@"symptoms"],
                               @"ReasonAppt"    : responseObject[@"detail_data"][@"reason_for_appointment"],
                               @"Phone"         : responseObject[@"detail_data"][@"phone"],
                               @"Patient_Id"    : responseObject[@"detail_data"][@"patient_id"],
                               @"Medium"        : responseObject[@"detail_data"][@"medium"],
                               @"Medicines_Taken":responseObject[@"detail_data"][@"medicines_taken"],
                               @"Medical_Conditions":responseObject[@"detail_data"][@"medical_conditions"],
                               @"Gender"        : responseObject[@"detail_data"][@"gender"],
                               @"Email_Id"      : responseObject[@"detail_data"][@"email_id"],
                               @"Consultation_Date": responseObject[@"detail_data"][@"consultation_date"],
                               @"Blood_Group"   : responseObject[@"detail_data"][@"blood_group"],
                               @"Allergy"       : responseObject[@"detail_data"][@"allergy"],
                               @"DOB"           : responseObject[@"detail_data"][@"date_of_birth"],
                               @"Section_Headers": @[@"Personal Details",@"Consultation Details",@"Actions"],
                               @"Sub_Headers":@[
                                       @[@"Number :",@"Email-id :",@"Blood Group :",@"Date of Birth :",@"Address :"],
                                       @[@"Consultation Date :",@"Period",@"Reason for Appointment",@"When Did You Start Feeling This Way? :",@"Allergy :"]
                                       ]
                               };
        }
            break;
            
        case PaidQuestion:{
            
            patientDetails = @{@"Patient_name"  : responseObject[@"result"][@"name"],
                               @"Profile_image" : responseObject[@"result"][@"profile_image"],
                               @"Phone"         : responseObject[@"result"][@"mobile"],
                               @"Patient_Id"    : responseObject[@"result"][@"patient_id"],
                               @"Gender"        : responseObject[@"result"][@"gender"],
                               @"Email_Id"      : responseObject[@"result"][@"email_id"],
                               @"Blood_Group"   : responseObject[@"result"][@"blood_group"],
                               @"Allergy"       : responseObject[@"result"][@"allergy"],
                               @"DOB"           : responseObject[@"result"][@"date_of_birth"],
                               @"Section_Headers": @[@"Personal Details",@"Question Details"]
                               };
        }
            break;
            
        case NextConsultation:{
            
        }
            break;
            
        case NextPediatricionCall:{
            
        }
            break;
    }
    [_tableView reloadData];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([expandedSections containsIndex:indexPath.section]) {
        
        [expandedSections removeIndex:indexPath.section];
        
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths : @[[NSIndexPath indexPathForRow:1
                                                               inSection:indexPath.section]]
                         withRowAnimation : UITableViewRowAnimationFade];
        [tableView endUpdates];
        
    } else {
        
        [expandedSections addIndex:indexPath.section];
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths : @[[NSIndexPath indexPathForRow : 1
                                                                 inSection : indexPath.section]]
                         withRowAnimation : UITableViewRowAnimationFade];
        [tableView endUpdates];
    }
}
@end
