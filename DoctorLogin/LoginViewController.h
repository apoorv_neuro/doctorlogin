//
//  LoginViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 29/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"

#import "AppDelegate.h"
#import "CustomAlertController.h"
#import "PersistencyManager.h"

@interface LoginViewController : UIViewController

@end
