//
//  PatientDetailTableViewCell.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 15/06/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PatientDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_0;
@property (weak, nonatomic) IBOutlet UILabel *lbl_1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_3;
@property (weak, nonatomic) IBOutlet UILabel *lbl_4;

@property (weak, nonatomic) IBOutlet UILabel *lbl_11;
@property (weak, nonatomic) IBOutlet UILabel *lbl_12;
@property (weak, nonatomic) IBOutlet UILabel *lbl_13;
@property (weak, nonatomic) IBOutlet UILabel *lbl_14;
@property (weak, nonatomic) IBOutlet UILabel *lbl_15;

@end
