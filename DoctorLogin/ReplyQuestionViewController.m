//
//  ReplyQuestionViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 13/07/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "ReplyQuestionViewController.h"
#import "AppDelegate.h"
#import "CustomAlertController.h"
#import <SVProgressHUD.h>
#import <AFNetworking.h>
#import "PersistencyManager.h"


@interface ReplyQuestionViewController ()

@property (weak, nonatomic) IBOutlet UITextView *txtViewQuestion;
@property (weak, nonatomic) IBOutlet UITextView *txtViewAnswer;

@end

@implementation ReplyQuestionViewController
//question_id * , doctor_id * , reply * , segment * ,  ( segment = 'my-questions' or 'open-questions')
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Question Data ----> %@",_questionData);
    
    _txtViewQuestion.text = _questionData[@"question"];
}
- (IBAction)fadeViewTouched:(id)sender {
    
    [self dismissViewControllerAnimated : YES
                             completion : nil];
}
- (IBAction)bttnClosePressed:(id)sender {
    
    [self dismissViewControllerAnimated : YES
                             completion : nil];
}
- (IBAction)bttnReplyPressed:(id)sender {
    
    if ([_txtViewAnswer.text isEqualToString:@""]) {
        
        [CustomAlertController showAlertOnController:self
                                       withAlertType:Simple
                                       andAttributes:@{
                                                       @"title" : @"Sorry!",
                                                       @"message" :@"Please Enter Your Answer First"
                                                       }];
    } else {
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        
        if (appDelegate.currentInternetStatus) {
            
            [SVProgressHUD show];

            NSString *urlAsString = [NSString stringWithFormat:@"%@reply_post",BASE_URL];
            
            NSLog(@"Free Question Reply API ----> %@",urlAsString);
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSDictionary *parameters = @{@"doctor_id":[PersistencyManager getDoctorId] ,
                                         @"reply":_txtViewAnswer.text,
                                         @"question_id":_questionData[@"question_id"],
                                         @"segment":@"open-questions"
                                         };
            
            NSLog(@"Free Question Reply Parameters ----> %@",parameters);
            
            [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
            
            [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                       password : @"Neuro@123"];
            
            [manager.requestSerializer setTimeoutInterval:10];
            
            [manager GET:urlAsString
              parameters:parameters
                progress:nil
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     
                     [SVProgressHUD dismiss];

                     if (responseObject != nil && responseObject != NULL) {
                         
                         
                         NSLog(@"Response Data ----------------->\n%@",responseObject);
                         if (responseObject[@"success"] == 1) {
                             
                             [CustomAlertController showAlertOnController:self
                                                            withAlertType:Simple
                                                            andAttributes:@{
                                                                            @"title" : @"No Data Received",
                                                                            @"message" : @"Server Is Not Responding"
                                                                            }];
                         }
                         
                     } else {
                         
                         [CustomAlertController showAlertOnController:self
                                                        withAlertType:Simple
                                                        andAttributes:@{
                                                                        @"title" : @"No Data Received",
                                                                        @"message" : @"Server Is Not Responding"
                                                                        }];
                     }
                 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     
                     [SVProgressHUD dismiss];

                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"Error",
                                                                    @"message" :@"Request Timed Out"
                                                                    }];
                 }];
        } else {
            
            [CustomAlertController showAlertOnController : self
                                           withAlertType : Network
                                           andAttributes : nil];
        }
    }
}
@end