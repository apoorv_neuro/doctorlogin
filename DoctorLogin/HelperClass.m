//
//  HelperClass.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 05/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "HelperClass.h"

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

@implementation HelperClass

+(CGFloat)getStringWidthWithString:(NSString*)string andFont:(UIFont*)font {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    
    CGRect stringBoundingBox = [string boundingRectWithSize:CGSizeMake(screenBounds.size.width - 60, 100)
                                                    options:NSStringDrawingUsesLineFragmentOrigin
                                                 attributes:@{NSFontAttributeName: font}
                                                    context:nil];
    return stringBoundingBox.size.height;
}

+(int)getPickerIndexWithArray:(NSArray*)array andString:(NSString*)string {
    
    for (int i = 0; i < array.count; i++) {
        
        if ([array[i] isKindOfClass:[NSString class]]) {
           
            if ([array[i] isEqualToString:string]) {

                return i;
            }
        }
    }
    return 0;
}
+(int)getPickerIndexWithArray:(NSArray*)array associatedKey:(NSString*)key andString:(NSString*)string {
   
    NSInteger selectedIndex = 0;
    for (NSDictionary *dict in array) {
        
        if ([dict[key] isEqualToString:string]) {
            
            return selectedIndex;
        }
        selectedIndex++;
    }
    return 0;
}
+ (BOOL)validateEmailAddress:(NSString *)emailId {
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailId];
}
+ (BOOL)validateMobileNumber:(NSString *)mobileNumber {
    
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    BOOL matched = [mobileNumberPred evaluateWithObject:mobileNumber];
    return matched;
}
@end


#pragma mark - Validated Text Field Methods Implementation

@implementation ValidatedTextField

-(BOOL)textField:(UITextField *)textField
        shouldChangeCharactersInRange:(NSRange)range
        replacementString:(NSString *)string {
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        
        return NO;
    }
    
    if (range.location == textField.text.length
        &&
        ([string isEqualToString:@" "]
         &&
         [[textField.text substringFromIndex:textField.text.length-1] isEqualToString:@" "])
        ) {
        
        return NO;
    }
    
    if ([textField.placeholder isEqualToString:@"First Name"] || [textField.placeholder isEqualToString:@"Last Name"] || [textField.placeholder isEqualToString:@"Mobile Number"]) {
        
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        
        return [string isEqualToString:filtered];
    }
    
    if ([textField.placeholder isEqualToString:@"Email Id"] && [string isEqualToString:@" "]) {
        
        return NO;
    }
    return YES;
}
@end