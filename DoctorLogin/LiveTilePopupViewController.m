//
//  LiveTilePopupViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 30/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "LiveTilePopupViewController.h"
#import "AppDelegate.h"
#import <UIImageView+AFNetworking.h>
#import <AFNetworking.h>
#import "PersistencyManager.h"
#import <SVProgressHUD.h>


@interface LiveTilePopupViewController () {
    
    CGRect targetFrame;
    BOOL showSchedulePopUp;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgViewMedium;
@end
@implementation LiveTilePopupViewController

-(void)viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    if (_popUpContentMode == OnlineConsultation) {
        
        targetFrame = _mainView.frame;
        
    } else {
        
        targetFrame = _viewPaidQuestion.frame;
    }
}
-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    
    // Pre-Processing
    
    showSchedulePopUp = NO;
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [_mainView setHidden:YES];
    [_viewPaidQuestion setHidden:YES];
    
    switch (_popUpContentMode) {
            
        case OnlineConsultation:{
            
            NSLog(@" Mode = Online Consultation \n Popup Data --->>> %@",appDelegate.dashboardData[@"new_consultation"]) ;
            
            _lblUser.text = appDelegate.dashboardData[@"new_consultation"][0][@"patient_name"];
                        
            if ([appDelegate.dashboardData[@"new_consultation"][0][@"consultation_date"]isEqualToString:@"0000-00-00"]) {
                
                _lblDate.text = [appDelegate.dashboardData[@"new_consultation"][0][@"period_selected"]uppercaseString];
                
            } else {
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
                
                NSDate *tempDate = [formatter dateFromString:[NSString stringWithFormat:@"%@ %@",appDelegate.dashboardData[@"new_consultation"][0][@"consultation_date"],appDelegate.dashboardData[@"new_consultation"][0][@"consultation_time"]]];
                
                _lblDate.text = [self setDate:tempDate];
                
            }
            
            [_imageViewPopUp setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://logintohealth.com/demo/assets/uploads/patients/%@",appDelegate.dashboardData[@"new_consultation"][0][@"profile_image"]]]
                            placeholderImage:[UIImage imageNamed:@"placeholder"]];
            
            if ([appDelegate.dashboardData[@"new_consultation"][0][@"medium"] isEqualToString:@"video"]) {
                
                [_imgViewMedium setImage:[UIImage imageNamed:@"video"]];
                
            } else {
                
                [_imgViewMedium setImage:[UIImage imageNamed:@"mobile"]];
            }
        }
            break;
            
        case PaidQuestion:{
            
            //[appDelegate.dashboardData[@"next_paid_question"]
            
            [_imageViewPaidQuestion setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://logintohealth.com/demo/assets/uploads/patients/%@",appDelegate.dashboardData[@"next_paid_question"][0][@"profile_image"]]]
                                   placeholderImage:[UIImage imageNamed:@"placeholder"]];
            
            _lblPaidQuestPatientName.text = appDelegate.dashboardData[@"next_paid_question"][0][@"name"];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
            
            NSDate *tempDate = [formatter dateFromString:[NSString stringWithFormat:@"%@ ",appDelegate.dashboardData[@"next_paid_question"][0][@"updated"]]];
            
            _lblPaidQUestionDate.text = [self setDate:tempDate];
            _lblPaidQuestion.text = appDelegate.dashboardData[@"next_paid_question"][0][@"question"];
            
        }
            break;
        default:
            break;
    }
}
-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    switch (_popUpContentMode) {
            
        case OnlineConsultation:
            
            [self showPopUpWithView:_mainView];
            
            break;
        case PaidQuestion:
            
            [self showPopUpWithView:_viewPaidQuestion];
            
            break;
            
            default:
            break;
    }
}
-(void)showPopUpWithView:(UIView*)popUpView {
    
    [popUpView setFrame:CGRectMake (_initialFrame.origin.x,_initialFrame.origin.y+64, _initialFrame.size.width, _initialFrame.size.height)];
    
    [popUpView setHidden:NO];
    
    NSLog(@"Cell Frame ----> %@",NSStringFromCGRect(_initialFrame));
    
    [UIView animateWithDuration:0.4
                          delay:0.0
         usingSpringWithDamping:0.6
          initialSpringVelocity:0.3
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         popUpView.frame = targetFrame;
                         
                         [_fadeView setAlpha:0.699999988079071];
                         
                     } completion:^(BOOL finished) {
                         
                         UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                                  action:@selector(dismissPopUp:)];
                         
                         [_fadeView addGestureRecognizer:gesture];
                     }];
}
- (void)dismissPopUp:(id *)gesture {
    
    UIView *popUpView = _popUpContentMode == OnlineConsultation ? _mainView:_viewPaidQuestion;
    
    
    [UIView animateWithDuration:0.4
                          delay:0.0
         usingSpringWithDamping:1
          initialSpringVelocity:1
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         
                         [_fadeView setAlpha:0];
                         [popUpView setAlpha:0];

                         popUpView.frame = CGRectMake (_initialFrame.origin.x
                                                       ,_initialFrame.origin.y+64
                                                       ,_initialFrame.size.width
                                                       ,_initialFrame.size.height);
                         
                     } completion:^(BOOL finished) {
                         
                         [self dismissViewControllerAnimated:NO
                                                  completion:^(void){
                                                      
                                                      if (showSchedulePopUp) {
                                                        
                                                          [self.delegate showSchedulePopUp];
                                                      }
                                                  }];
                     }];
}

-(NSString*)setDate:(NSDate*)myDate {
    
    NSDateFormatter *prefixDateFormatter = [[NSDateFormatter alloc] init] ;
    [prefixDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [prefixDateFormatter setDateFormat:@"DD"];
    
    NSString *prefixDateString = [prefixDateFormatter stringFromDate:myDate];
    
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init] ;
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [monthDayFormatter setDateFormat:@"d"];
    
    int date_day = [[monthDayFormatter stringFromDate:myDate] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [suffixes objectAtIndex:date_day];
    NSString *dateString = [prefixDateString stringByAppendingString:suffix];
    
    [prefixDateFormatter setDateFormat:@" MMM | "];
    
    dateString = [dateString stringByAppendingString:[prefixDateFormatter stringFromDate:myDate]];
    
    [prefixDateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    dateString = [dateString stringByAppendingString:[prefixDateFormatter stringFromDate:myDate]];
    return dateString;
}
- (IBAction)bttnViewProfilePressed:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(popUpDidDismiss:)]) {

        [self.delegate popUpDidDismiss:_popUpContentMode];
    }
    
    [self dismissPopUp:nil];
}

- (IBAction)bttnAcceptPressed:(id)sender {
    
    switch (_popUpContentMode) {
            
        case OnlineConsultation: {
            
            showSchedulePopUp = YES;
            
            [self dismissPopUp:nil];
        }
            break;
        case PaidQuestion: {
            //accept
            [self acceptPaidQuestion];
        }
            break;
        default:
            break;
    }
    
}
- (IBAction)bttnCancelPressed:(id)sender {
    
}
#pragma  mark - Calling API's

-(void)acceptPaidQuestion
{
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentInternetStatus) {
        
        [SVProgressHUD show];

        NSString *urlAsString = [NSString stringWithFormat:@"%@accept",BASE_URL];
        
        NSLog(@"Paid Question Accept API ----> %@",urlAsString);
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSDictionary *parameters = @{
                                     @"doctor_id"   : [PersistencyManager getDoctorId],
                                     @"question_id" : appDelegate.dashboardData[@"next_paid_question"][0][@"question_id"]
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        [manager.requestSerializer setTimeoutInterval:10];
        
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
              
                 [SVProgressHUD dismiss];

                 if (responseObject != nil && responseObject != NULL) {
                     
                     NSLog(@"Paid Question Accept API ----> %@",responseObject);
                     
                     [self dismissPopUp:nil];
                     
                     if ([self.delegate respondsToSelector:@selector(refreshData)]) {
                         
                         [self.delegate refreshData];
                     }
                 }
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

                 [SVProgressHUD dismiss];
                 
                 NSLog(@"Paid Question Accept API --- Error |||_______---->>>%@",[error localizedDescription]);
             }];
    }
}
@end