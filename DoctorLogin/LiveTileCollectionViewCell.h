//
//  LiveTileCollectionViewCell.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 27/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveTileCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPatient;
@property (weak, nonatomic) IBOutlet UILabel *labelPatient;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;
@property (weak, nonatomic) IBOutlet UILabel *labelHeading;

@end
