//
//  HelperClass.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 05/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface HelperClass : NSObject

+ (int)getPickerIndexWithArray:(NSArray*)array andString:(NSString*)string;
+ (int)getPickerIndexWithArray:(NSArray*)array associatedKey:(NSString*)key andString:(NSString*)string;

+ (BOOL)validateEmailAddress:(NSString *)emailId;
+ (BOOL)validateMobileNumber:(NSString *)mobileNumber;

+(CGFloat)getStringWidthWithString:(NSString*)string andFont:(UIFont*)font;

@end

@interface ValidatedTextField : NSObject < UITextFieldDelegate >
@end
