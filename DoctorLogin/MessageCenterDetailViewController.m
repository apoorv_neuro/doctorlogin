//
//  MessageCenterDetailViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 24/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "MessageCenterDetailViewController.h"
#import "MessageTimeSlotsTableViewCell.h"
#import "ConsultationsDetailTableViewCell.h"

@interface MessageCenterDetailViewController () < UITableViewDelegate , UITableViewDataSource > {
    
    NSMutableIndexSet *expandedSections;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation MessageCenterDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    expandedSections = [NSMutableIndexSet new];
    
    NSLog(@"Detail Data%@",_timeSlotArray);
    
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 64;
    
}
-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}
#pragma mark - UITableview Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return _timeSlotArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([expandedSections containsIndex:section]) {
        
        return 2;
    }
    
    return 1;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row > 0) {
        
        ConsultationsDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"consultationDetailCell" forIndexPath:indexPath];
        
        [cell.bttnViewDetails setTag:indexPath.row];
        
        [cell.bttnViewDetails addTarget:self action:@selector(bttnViewDetailsPressed) forControlEvents:UIControlEventTouchUpInside];
        
        if ([_timeSlotArray[indexPath.section][@"medium"]isEqualToString:@"video"]) {
            
            [cell.imageViewMedium setImage:[UIImage imageNamed:@"video"]] ;
            
        } else {
            
            [cell.imageViewMedium setImage:[UIImage imageNamed:@"mobile"]] ;
        }
        cell.lblPatientName.text = _timeSlotArray[indexPath.section][@"patient_name"];
        
        cell.lblMediumOfCommunication.text = _timeSlotArray[indexPath.section][@"medium"];
        
        cell.lblRequestType.text = _timeSlotArray[indexPath.section][@"consultation_type"];
        
        cell.lblReasonForConsultation.text = _timeSlotArray[indexPath.section][@"reason_for_appointment"];
        
        CGRect initialLabelFrame = cell.lblReport.frame;
        CGRect initialImageFrame = cell.imageViewTrash.frame;
        
        if ([_timeSlotArray[indexPath.section][@"reports"]count] > 0) {
            
            if (cell.reportsHeightConstraint.constant <= 40) {
                
                [cell.imageViewTrash setHidden:NO];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(deleteReportPressed:)];
                
                [cell.imageViewTrash addGestureRecognizer:tap];
                
                cell.lblReport.text = _timeSlotArray[indexPath.section][@"reports"][0][@"attachment"];
                
                for (int i = 1 ; i < [_timeSlotArray[indexPath.section][@"reports"]count] ; i++) {
                    
                    initialLabelFrame.origin.y = initialLabelFrame.origin.y + initialLabelFrame.size.height + 5;
                    initialImageFrame.origin.y = initialImageFrame.origin.y + initialImageFrame.size.height + 5;
                    
                    UILabel *label = [[UILabel alloc]initWithFrame:initialLabelFrame];
                    
                    [label setNumberOfLines:0];
                    
                    [label setTextColor:cell.lblReport.textColor];
                    
                    [label setFont:cell.lblReport.font];
                    
                    label.text = _timeSlotArray[indexPath.section][@"reports"][i][@"attachment"];
                    
                    [label setTag:i];
                    [cell.viewReports addSubview:label];
                    
                    UIImageView *imageView = [[UIImageView alloc]initWithFrame:initialImageFrame];
                    
                    [imageView setImage:cell.imageViewTrash.image];
                    [imageView setUserInteractionEnabled:YES];
                    [imageView setTag:i+1];
                    
                    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(deleteReportPressed:)];

                    [imageView addGestureRecognizer:tap];
                    
                    [cell.viewReports addSubview:imageView];
                }
                
                  [cell.reportsHeightConstraint setConstant:40 + 10 + (initialLabelFrame.size.height*[_timeSlotArray[indexPath.section][@"reports"]count])];
            }
        } else {
            
            cell.lblReport.text = @"No Reports";
            [cell.reportsHeightConstraint setConstant:40];
            
            for (UIView *view in cell.viewReports.subviews) {
                
                if (view.tag != 0) {
                    
                    if ([view isKindOfClass:[UIImageView class]]) {
                        
                        [view setHidden:YES];
                        
                    } else {
                        
                        [view removeFromSuperview];
                    }
                }
            }
        }
        return cell;
        
    } else {
        
        static NSString *cellId = @"headerCell";
        
        MessageTimeSlotsTableViewCell *headerCell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
        
        if ([expandedSections containsIndex:indexPath.section]) {
            
                headerCell.arrow.transform = CGAffineTransformMakeRotation(M_PI/2);
        }
        else {
            
                headerCell.arrow.transform = CGAffineTransformMakeRotation(0);
        }

        
        if (![_timeSlotArray[indexPath.section][@"consultation_date"]isKindOfClass:[NSString class]]) {
            
            headerCell.labelDate.text = [_timeSlotArray[indexPath.section][@"period_selected"]uppercaseString];
            
        } else {
            
            headerCell.labelDate.text = [NSString stringWithFormat:@"%@",_timeSlotArray[indexPath.section][@"consultation_date"]];
        }
        
        headerCell.labelTime.text = [NSString stringWithFormat:@"%@",_timeSlotArray[indexPath.section][@"consultation_time"]];
        
        return headerCell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
      
        BOOL currentlyExpanded = [expandedSections containsIndex:indexPath.section];
        
        MessageTimeSlotsTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

        if (currentlyExpanded) {
            
            [expandedSections removeIndex:indexPath.section];
            
            [tableView beginUpdates];
            NSIndexPath *index = [NSIndexPath indexPathForRow:1
                                                    inSection:indexPath.section];
            [tableView deleteRowsAtIndexPaths:@[index]
                             withRowAnimation:UITableViewRowAnimationTop];
            
            [tableView endUpdates];
            
            [UIView animateWithDuration:0.5 animations:^{
                
                cell.arrow.transform = CGAffineTransformMakeRotation(0);
                
            }];
            
        } else {
            
            NSInteger expandedsection;
            NSIndexPath *deleteIndex;
            
            [UIView animateWithDuration:0.5 animations:^{
                
                cell.arrow.transform = CGAffineTransformMakeRotation(M_PI/2);
                
            }];
            BOOL count = expandedSections.count;
            
            if (count) {
                
                expandedsection = [expandedSections firstIndex];
                deleteIndex = [NSIndexPath indexPathForRow:1 inSection:expandedsection];
                
                [expandedSections removeIndex:expandedsection];
                
                cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:expandedsection]];
                
                [UIView animateWithDuration:0.5 animations:^{
                    
                    cell.arrow.transform = CGAffineTransformMakeRotation(0);
                    
                }];

            }
            
            NSIndexPath *addindex = [NSIndexPath indexPathForRow:1
                                                       inSection:indexPath.section];
            [expandedSections addIndex:indexPath.section];
            
            [tableView beginUpdates];
            
            if (count) {
                
                [tableView deleteRowsAtIndexPaths:@[deleteIndex]
                                 withRowAnimation:UITableViewRowAnimationTop];
            }
            
            [tableView insertRowsAtIndexPaths:@[addindex]
                             withRowAnimation:UITableViewRowAnimationTop];
            [tableView endUpdates];
            
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([expandedSections containsIndex:indexPath.section]) {
        
        return 220;
        
    } else {
        
        return 60;
    }
}

#pragma mark - Custom Selector Methods

-(void)bttnViewDetailsPressed:(id)sender {
    
}
-(void)deleteReportPressed:(UITapGestureRecognizer*)tapGesture {
    
     NSLog(@"Tap Received With Tag --> %ld",[[tapGesture view] tag]);
}
@end