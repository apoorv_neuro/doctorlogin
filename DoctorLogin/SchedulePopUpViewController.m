//
//  SchedulePopUpViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 16/06/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "SchedulePopUpViewController.h"
#import "AppDelegate.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import "CustomAlertController.h"
#import "PersistencyManager.h"

@interface SchedulePopUpViewController () < UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate > {
    
    UIPickerView *timePickerView;
    NSMutableArray *timeSlots;
}
@property (weak, nonatomic) IBOutlet UIView *viewDate;
@property (weak, nonatomic) IBOutlet UIView *viewSlot;

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblSlot;

@property (weak, nonatomic) IBOutlet UITextField *txtFldSelectDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFldTimeSlots;
@end

@implementation SchedulePopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    [_viewDate.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewDate.layer setBorderWidth:0.5];
    
    [_viewSlot.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_viewSlot.layer setBorderWidth:0.5];
    
    _lblSlot.text = [delegate.dashboardData[@"new_consultation"][0][@"period_selected"] uppercaseString];
    _lblDate.text = delegate.dashboardData[@"new_consultation"][0][@"consultation_date"];
    
    // Setting Pickers
    UIDatePicker * datePicker = [[UIDatePicker alloc] init];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setMinimumDate:[NSDate date]];
    [datePicker addTarget:self
                   action:@selector(datePickerDidSelectDate:)
         forControlEvents:UIControlEventValueChanged];
    
    [_txtFldSelectDate setInputView:datePicker];
    
    timeSlots = [NSMutableArray new];
    
    timePickerView = [[UIPickerView alloc]init];
    timePickerView.dataSource = self;
    timePickerView.delegate = self;
    _txtFldTimeSlots.inputView = timePickerView;
    
    _txtFldTimeSlots.delegate = self;
    [_txtFldSelectDate setPlaceholder:[NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle]];
}

-(void)datePickerDidSelectDate:(UIDatePicker *)datePicker {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    NSString *stringFromDate = [formatter stringFromDate:datePicker.date];
    _txtFldSelectDate.text = stringFromDate;
    [self getTimeSlots];
}
- (IBAction)fadeViewTouched:(id)sender {
    
    [self dismissViewControllerAnimated : YES
                             completion : nil];
}
- (IBAction)bttnSchedulePressed:(id)sender {
    
    if ([_txtFldSelectDate.text isEqualToString:@""]
        ||
        [_txtFldTimeSlots.text isEqualToString:@""]) {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Simple
                                       andAttributes : @{
                                                         @"title" : @"Incomplete Details",
                                                         @"message" : @"Please Complete The Required Details First"
                                                         }];
    } else {
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;

        if (appDelegate.currentInternetStatus) {
            
            [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
            
            NSString *urlAsString = [NSString stringWithFormat:@"%@web_doctor_confirm_consultation_schedule", BASE_URL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSDictionary *parameters = @{@"doctor_id" :[PersistencyManager getDoctorId] ,
                                         @"schedule_date": _txtFldSelectDate.text,
                                         @"consultation_time":_txtFldTimeSlots.text,
                                         @"patient_id":appDelegate.dashboardData[@"new_consultation"][0][@"patient_id"],
                                         @"consultation_id":appDelegate.dashboardData[@"new_consultation"][0][@"consultation_id"]
                                         };
            
            [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
            [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                       password : @"Neuro@123"];
            [manager GET:urlAsString
              parameters:parameters
                progress:nil
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     
                     [SVProgressHUD dismiss];
                     
                     NSLog(@" Slot Confirmation API --- Response---->%@",responseObject);
                     
                     if (responseObject != nil && responseObject != NULL) {
                         
                         if ([responseObject[@"message"] isKindOfClass:[NSString class]]) {
                             
                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Success!"
                                                                                                      message:responseObject[@"message"]
                                                                                               preferredStyle:UIAlertControllerStyleAlert];
                             
                             
                             UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                                  
                                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                              }];
                             
                             [alertController addAction:okAction];
                             [self presentViewController:alertController
                                                animated:YES
                                              completion:Nil];
                         }
                    } else {
                        
                         [CustomAlertController showAlertOnController:self
                                                        withAlertType:Simple
                                                        andAttributes:@{
                                                                        @"title" : @"No Data Received",
                                                                        @"message" : @"Server Is Not Responding"
                                                                        }];
                     }
                 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     
                     [SVProgressHUD dismiss];
                     
                     NSLog(@"Slot Confirmation API ---  Error |||_______---->>>%@",[error localizedDescription]);
                 }];
        } else {
            
            [CustomAlertController showAlertOnController : self
                                           withAlertType : Network
                                           andAttributes : nil];
        }
    }
}
-(void)getTimeSlots {

    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (![_txtFldSelectDate.text isEqualToString:@""] && _txtFldSelectDate.text != nil) {
        
        if (appDelegate.currentInternetStatus) {
            
            NSString *urlAsString = [NSString stringWithFormat:@"%@web_doctor_confirm_consultation_time_slot_for_schedule_edit", BASE_URL];
            
            AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
            
            NSDictionary *parameters = @{@"doctor_id" : [PersistencyManager getDoctorId] ,
                                         @"sch_date": _txtFldSelectDate.text
                                         };
            
            [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
            
            [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                       password : @"Neuro@123"];
            [manager GET:urlAsString
              parameters:parameters
                progress:nil
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     
                     NSLog(@" Get Time Slots API --- Response---->%@",responseObject);
                     
                     if (responseObject != nil && responseObject != NULL) {
                         
                         for (NSDictionary *dict in responseObject[@"all_slot"]) {
                           
                             [timeSlots addObject:dict[@"value"]];

                         }
                     } else {
                         
                         [CustomAlertController showAlertOnController:self
                                                        withAlertType:Simple
                                                        andAttributes:@{
                                                                        @"title" : @"No Data Received",
                                                                        @"message" : @"Server Is Not Responding"
                                                                        }];
                         
                         [self dismissViewControllerAnimated:YES completion:nil];
                     }
                     
                 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                     
                     NSLog(@"Get Time Slots API ---  Error |||_______---->>>%@",[error localizedDescription]);
                 }];
        } else {
            
            [CustomAlertController showAlertOnController : self
                                           withAlertType : Network
                                           andAttributes : nil];
        }
    } else {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Simple
                                       andAttributes : @{
                                                       @"title" : @"Incomplete Details",
                                                       @"message" : @"Please Select A Date First"
                                                       }];
    }
}
#pragma mark - Picker View DataSource Methods

-(NSString*)pickerView:(UIPickerView*)pickerView
            titleForRow:(NSInteger)row
           forComponent:(NSInteger)component {
    
    return timeSlots[row];
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
   
    return 1;
}
-(void)pickerView:(UIPickerView *)pickerView
     didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component {

    if (timeSlots.count > 0) {
        
        _txtFldTimeSlots.text = timeSlots[row];
    }
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return timeSlots.count;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (timeSlots.count == 0) {
      
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Simple
                                       andAttributes : @{
                                                         @"title" : @"Incomplete Details",
                                                         @"message" : @"Please Select A Date First"
                                                         }];
        return NO;
    }
    
    return YES;
}
@end