//
//  RegisterViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 29/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HelperClass.h"

@interface RegisterViewController : UIViewController

@property (strong , nonatomic) id responseData;
@property (nonatomic) int loginTag;
@end
