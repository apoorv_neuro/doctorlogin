//
//  LoginViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 29/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import "HelperClass.h"


@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtFldEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtFldPassword;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    
    [_txtFldPassword addTarget:self action:@selector(textFieldDidChange) forControlEvents:UIControlEventEditingChanged];
}
#pragma mark - Login

- (IBAction)buttonLoginPressed:(id)sender {
    
    if ([_txtFldPassword.text isEqualToString:@""] || [_txtFldEmail.text isEqualToString:@""]) {
        
        [self  validateEntries];
        
    } else {
        
        if ([HelperClass validateEmailAddress:_txtFldEmail.text]) {
          
            AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            
            // Check Connection
            
            if (appDelegate.currentInternetStatus) {
                
                [SVProgressHUD showWithStatus:@"Please Wait.." maskType:SVProgressHUDMaskTypeGradient];
                
                AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                
                NSString *urlAsString = [NSString stringWithFormat:@"%@doctorlogin",BASE_URL];
                
                NSDictionary *parameters = @{@"email_id" : _txtFldEmail.text ,
                                             @"password" : _txtFldPassword.text
                                             };
                
                [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
                
                [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername:@"kushals@neuronimbus.com"
                                                                           password:@"Neuro@123"];
                [manager GET : urlAsString
                  parameters : parameters
                    progress : nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         [SVProgressHUD dismiss];
                         
                         NSLog(@" Login API --- Response---->%@",responseObject);
                         
                         if (responseObject != nil && responseObject != NULL) {
                             
                             if ([responseObject[@"message"] isKindOfClass:[NSArray class]]) {
                                 
                                   
                                 if ([responseObject[@"message"][0] isKindOfClass:[NSString class]]) {
                                     
                                     [CustomAlertController showAlertOnController:self
                                                                    withAlertType:Simple
                                                                    andAttributes:@{
                                                                                    @"title" : @"Error",
                                                                                    @"message" : responseObject[@"message"][0]
                                                                                    }];
                                 }
                                 // Case : Success
                                 
                                 if ([responseObject[@"message"][0] isKindOfClass:[NSDictionary class]]) {
                                     
                                     // Call Persistency Manager Here
                                     
                                     [PersistencyManager loginWithDoctorId : responseObject[@"message"][0][@"doctor_id"]];
                                     [appDelegate showDashboard];
                                 }
                             }
                         } else {
                             
                             // When The Server Returns NULL
                             
                             [CustomAlertController showAlertOnController:self
                                                            withAlertType:Simple
                                                            andAttributes:@{
                                                                            @"title" : @"No Data Received",
                                                                            @"message" : @"Server Is Not Responding"
                                                                            }];
                         }
                         
                     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         
                         [SVProgressHUD dismiss];
                         
                         NSLog(@"Login API --- Error |||_______---->>>%@",[error localizedDescription]);
                         
                         [CustomAlertController showAlertOnController:self
                                                        withAlertType:Simple
                                                        andAttributes:@{
                                                                        @"title" : @"Error",
                                                                        @"message" :@""
                                                                        }];
                     }];
            } else {
                
                [CustomAlertController showAlertOnController:self
                                               withAlertType:Network
                                               andAttributes:nil];
            }
        } else {
            
            [CustomAlertController showAlertOnController:self
                                           withAlertType:Simple
                                           andAttributes:@{
                                                           @"title" : @"Invalid Email",
                                                           @"message" :@"Please Enter A Valid E-Mail Address"
                                                           }];
        }
        
    }
}
#pragma mark - Login API Response On 6th May

/* Response Object ---->{
 
 Case 1:  Logged In Successfully
 
 message =     (
 {
 "doctor_id" = 38;
 }
 );
 success = 1;
 
 Case 2 : Incorrect Credentials
 
 {
 message =     (
 "Incorrect Email Id or Password."
 );
 success = 0;
 }
 }*/

#pragma mark - Forgot Password

- (IBAction)forgotPasswordButtonPressed:(id)sender {
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"Forgot Password !"
                                                                        message:@"Enter Your Email Id "
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [controller addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        
        UIView *container = [[textField superview] superview];
        container.layer.cornerRadius = 5;
        textField.placeholder=@"Enter Your Email-Id";
        
        if (self.txtFldEmail.text != nil) {
            
            textField.text = self.txtFldEmail.text;
        }
    }];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Submit"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action) {
        
                                                   
        
        UITextField *textField = (UITextField*)controller.textFields.firstObject;
        
        if ( textField.text != nil && ![textField.text isEqualToString:@""]) {
            
            if ([HelperClass validateEmailAddress:textField.text]) {
              
                AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
                
                if (appDelegate.currentInternetStatus) {
                    
                    [SVProgressHUD showWithStatus:@"Please Wait.."
                                         maskType:SVProgressHUDMaskTypeGradient];
                    
                    NSString *urlAsString = [NSString stringWithFormat:@"%@webdoctorforgetpass", BASE_URL];
                    
                    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                    
                    NSDictionary *parameters = @{@"email_id" : _txtFldEmail.text ,
                                                 };
                    
                    [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
                    
                    [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                               password : @"Neuro@123"];
                    [manager GET:urlAsString
                      parameters:parameters
                        progress:nil
                         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                             
                             [SVProgressHUD dismiss];
                             
                             NSLog(@" Forgot Password API --- Response---->%@",responseObject);
                             
                             if (responseObject != nil && responseObject != NULL) {
                                 
                                 [CustomAlertController showAlertOnController:self
                                                                withAlertType:Simple
                                                                andAttributes:@{
                                                                                @"title" : @"Success",
                                                                                @"message" : responseObject[@"message"]
                                                                                }];
                             } else {
                                 
                                 // When The Server Returns NULL
                                 
                                 [CustomAlertController showAlertOnController:self
                                                                withAlertType:Simple
                                                                andAttributes:@{
                                                                                @"title" : @"No Data Received",
                                                                                @"message" : @"Server Is Not Responding"
                                                                                }];
                             }
                             
                         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                             
                             
                             [SVProgressHUD dismiss];
                             
                             NSLog(@"Forgot Password API --- Error |||_______---->>>%@",[error localizedDescription]);
                         }];
                } else {
                    
                    [CustomAlertController showAlertOnController:self
                                                   withAlertType:Network
                                                   andAttributes:nil];
                }
            } else {
                
                [CustomAlertController showAlertOnController:self
                                               withAlertType:Simple
                                               andAttributes:@{
                                                               @"title" : @"Please Enter A Valid Email-Id",
                                                               @"message" : @""
                                                               }];
            }
        } else {
            
            [CustomAlertController showAlertOnController:self
                                           withAlertType:Simple
                                           andAttributes:@{
                                                           @"title" : @"Please Enter Your Email-ID",
                                                           @"message" : @""
                                                           }];
        }
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [controller addAction:ok];
    [controller addAction:cancel];
    
    [self presentViewController:controller
                       animated:YES
                     completion:nil];
}
-(void)validateEntries {
    
    if ([_txtFldEmail.text isEqualToString:@""]) {
        
        [CustomAlertController showAlertOnController:self
                                       withAlertType:Simple
                                       andAttributes:@{
                                                       @"title" : @"Incomplete Details",
                                                       @"message" :@"Please Enter Your Email First"
                                                       }];
        
    } else {
        
        [CustomAlertController showAlertOnController:self
                                       withAlertType:Simple
                                       andAttributes:@{
                                                       @"title" : @"Incomplete Details",
                                                       @"message" :@"Please Enter Your Password"
                                                       }];
    }
}

#pragma mark - UITextFieldDelegate

-(void)textFieldDidChange {
    
    if (_txtFldPassword.text.length >= 16) {
        
        [_txtFldPassword resignFirstResponder];
    }
}
@end