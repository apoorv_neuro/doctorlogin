//
//  MessageTimeSlotsTableViewCell.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 24/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "MessageTimeSlotsTableViewCell.h"

@implementation MessageTimeSlotsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    _imageViewDate.image = [_imageViewDate.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_imageViewDate setTintColor:[UIColor whiteColor]];
    
    _imageViewTime.image = [_imageViewTime.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_imageViewTime setTintColor:[UIColor whiteColor]];


    _arrow.image = [_arrow.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_arrow setTintColor:[UIColor whiteColor]];
    
   
}
@end
