//
//  MessageCentreViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 23/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCentreViewController : UIViewController

typedef enum {
    
    OnlineConsultations,
    FreeQuestions,
    PaidQuestions,
    MissedCalls
    
} MessageCentreType;

typedef enum {
    
    Foreground,
    Background
    
}APICallMode;

@property (nonatomic) NSInteger selectedIndex;
@end
