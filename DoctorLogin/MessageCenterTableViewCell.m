//
//  MessageCenterTableViewCell.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 23/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "MessageCenterTableViewCell.h"

@implementation MessageCenterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _arrow.image = [_arrow.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_arrow setTintColor:[UIColor whiteColor]];
}
@end
