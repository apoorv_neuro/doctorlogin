//
//  ViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 28/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () < UIPageViewControllerDataSource >

@property (strong, nonatomic) UIPageViewController *pageController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle : UIPageViewControllerTransitionStyleScroll
                                                          navigationOrientation : UIPageViewControllerNavigationOrientationHorizontal
                                                                        options : nil];
    
    self.pageController.dataSource = self;
    
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    GuideViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:YES
                                 completion:nil];
    
    [self addChildViewController:self.pageController];
    
    [[self view] addSubview:[self.pageController view]];
    
    [self.pageController didMoveToParentViewController:self];
    
}

#pragma mark - UIPageViewController DataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(GuideViewController *)viewController index];
    
    if (index == 0 || index == 4) {
        
        return nil;
    }
    index--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(GuideViewController *)viewController index];
    
    index++;
    
    if (index == 9) {
        
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

#pragma mark - Custom Methods

- (GuideViewController *)viewControllerAtIndex:(NSUInteger)index {
    
    GuideViewController *childViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"guideView"];
    
    childViewController.index = index;
    
    return childViewController;
}

@end
