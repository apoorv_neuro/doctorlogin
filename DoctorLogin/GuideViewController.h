//
//  GuideViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 28/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideViewController : UIViewController
@property (assign, nonatomic) NSInteger index;
@end
