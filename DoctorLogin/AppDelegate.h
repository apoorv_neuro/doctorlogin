//
//  AppDelegate.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 28/04/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

#define BASE_URL @"https://logintohealth.com/demo/webservices/appdoctor/"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

// Reachability

@property (strong , nonatomic) Reachability *internetReachability;
@property (strong , nonatomic) Reachability *hostReachability;

@property (nonatomic)  BOOL currentInternetStatus;
@property (nonatomic)  BOOL currentHostStatus;


// Preloading Application Data

// 1. Cities
@property (strong , nonatomic) NSArray *cityArray;

// 2. States
@property (strong , nonatomic) NSArray *stateArray;

// 3. Specialisations
@property (strong , nonatomic) NSArray *specialisationArray;

// 4. Show Dashboard When Logged In

-(void)showDashboard;

// 5. Keep Dashboard Data In Cache

@property (strong , nonatomic) id dashboardData;

// Keep Message Center Data In Cache (Online Consultations)

@property (strong , nonatomic) id messageCenterData;

// Keep Message Center Data In Cache (Missed Calls)

@property (strong , nonatomic) id missedCallsData;

@end

