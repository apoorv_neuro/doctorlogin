//
//  ConsultationsDetailTableViewCell.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 08/06/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsultationsDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblPatientName;
@property (weak, nonatomic) IBOutlet UIButton *bttnStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestType;
@property (weak, nonatomic) IBOutlet UILabel *lblReasonForConsultation;
@property (weak, nonatomic) IBOutlet UILabel *lblMediumOfCommunication;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reportsHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *viewReports;
@property (weak, nonatomic) IBOutlet UILabel *lblReport;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewTrash;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMedium;
@property (weak, nonatomic) IBOutlet UIButton *bttnViewDetails;
@end
