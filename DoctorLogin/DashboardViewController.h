//
//  DashboardViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 09/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SchedulePopUpViewController.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface DashboardViewController : UIViewController {
    
    BOOL didPan ;
}
@property (weak, nonatomic) IBOutlet UIView *viewNotification;
@end
