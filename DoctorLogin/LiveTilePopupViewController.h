//
//  LiveTilePopupViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 30/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    
    OnlineConsultation,
    PaidQuestion,
    NextConsultation,
    NextPediatricionCall
    
} ContentMode;


@protocol PopUpDismissProtocol <NSObject>

-(void)popUpDidDismiss:(ContentMode)contentMode;
-(void)showSchedulePopUp;
-(void)refreshData;

@end

@interface LiveTilePopupViewController : UIViewController

// Common Entities
@property (nonatomic) CGRect initialFrame;
@property (nonatomic) ContentMode popUpContentMode;

// Online Consultation Elements

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *viewPaidQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblUser;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPopUp;
@property (weak, nonatomic) IBOutlet UILabel *popupTitle;
@property (weak, nonatomic) IBOutlet UIView *fadeView;

// Paid Question Elements
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPaidQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblPaidQuestPatientName;
@property (weak, nonatomic) IBOutlet UILabel *lblPaidQUestionDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPaidQuestion;

@property (weak , nonatomic) id <PopUpDismissProtocol> delegate;
@end
