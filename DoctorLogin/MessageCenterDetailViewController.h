//
//  MessageCenterDetailViewController.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 24/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCenterDetailViewController : UIViewController
@property (strong , nonatomic) NSArray *timeSlotArray;
@end