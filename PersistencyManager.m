//
//  PersistencyManager.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 06/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "PersistencyManager.h"
#import "AppDelegate.h"

@implementation PersistencyManager

#pragma mark - Login (Keeping Doctor Id For Current Session)

+ (void)loginWithDoctorId:(NSString*)doctorId {
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardDefaults setObject : doctorId
                         forKey : @"doctor_id"];
    
    [standardDefaults synchronize];
}

+ (NSString*)getDoctorId {
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    return [standardDefaults valueForKey:@"doctor_id"];
    
}

#pragma mark - Logout

+ (void)logout {
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    
    [standardDefaults removeObjectForKey:@"doctor_id"];
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    appDelegate.dashboardData = nil;
    appDelegate.messageCenterData = nil;
    appDelegate.missedCallsData = nil;
    [standardDefaults synchronize];
}

@end
