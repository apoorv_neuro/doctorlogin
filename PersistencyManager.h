//
//  PersistencyManager.h
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 06/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersistencyManager : NSObject

// Store Login Response i.e. doctor_id

+(void)loginWithDoctorId:(NSString*)doctorId;

// Logout From Current Session

+(void)logout;

// Get Doctor Id For Current Session

+(NSString*)getDoctorId;
@end
