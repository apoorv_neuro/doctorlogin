//
//  MenuItemsTVC.m
//  ObjCExample
//
//  Created by Evgeny on 09.01.15.
//  Copyright (c) 2015 Evgeny Nazarov. All rights reserved.
//

#import "MenuItemsTVC.h"
#import "DoctorLogin-Swift.h"
#import "PrecompiledMacros.h"
#import "AppDelegate.h"
#import "PersistencyManager.h"
#import <UIImageView+AFNetworking.h>

@implementation MenuItemsTVC

+ (instancetype)sharedInstance {
    
    static MenuItemsTVC *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [MenuItemsTVC new];
    });
    return sharedInstance;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    _imageArray = [NSArray arrayWithObjects:@"home-1"
                   ,@"profile"
                   ,@"country_information"
                   ,@"contact_us"
                   ,@"notification"
                   ,@"settings"
                   ,@"logout",@"",@"",@""
                   ,nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dashboardUpdateNotificationReceived) name:@"updateSlideoutMenu" object:nil];
}
-(void)dashboardUpdateNotificationReceived {
    
    [self.tableView reloadData];
}
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self setViewControllers];
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellID = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
        cell.backgroundColor = [UIColor clearColor];
        UIView *selectionView = [[UIView alloc] initWithFrame:cell.bounds];
        selectionView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3f];
        [cell setSelectedBackgroundView:selectionView];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
    }
    CALayer * layer = [CALayer layer];
    layer.frame = CGRectMake (5
                             ,cell.contentView.frame.size.height - 0.6
                             ,260
                             ,0.6);
    
    layer.backgroundColor = UIColorFromRGB(0xB1B1B1).CGColor;
    [cell.layer addSublayer:layer];
    
    [cell.textLabel setFont:[UIFont systemFontOfSize:15]];
    cell.textLabel.text = self.menuArray[indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.imageView.image = [UIImage imageNamed:_imageArray[indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 9) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirmation" message:@"Do You Want To Logout ?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [PersistencyManager logout];
            
            [[NSNotificationCenter defaultCenter] postNotificationName : @"logout"
                                                                object : nil];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    //    UINavigationController *navigationController = (UINavigationController*)[[[UIApplication sharedApplication].delegate window] rootViewController];
    //
    //       [navigationController hideSideMenuView];
    //
    //    BOOL searchTag = NO;
    //
    //    for (UIViewController *viewController in navigationController.viewControllers) {
    //
    //        if ([viewController isKindOfClass:viewControllers[indexPath.row]]) {
    //
    //            NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:navigationController.viewControllers];
    //
    //            [arr removeObjectIdenticalTo:viewController];
    //
    //            navigationController.viewControllers = arr;
    //
    //            [self pushControllerfAtIndex:indexPath.row
    //                withNavigationController:navigationController ];
    //
    //            searchTag = YES;
    //        }
    //    }
    //
    //    if (!searchTag) {
    //
    //            [self pushControllerfAtIndex:indexPath.row withNavigationController:navigationController];
    //    }
}

#pragma mark - Navigation

-(void)setViewControllers {
    
    //        viewControllers = @[@""
    //                            ,[MyProfileViewController class]
    //                            ,[CountryInformationViewController class]
    //                            ,[Contact_UsVc class]
    //                            ,[NotificationViewController class]
    //                            ,[MyProfileViewController class]
    //                            ,@""
    //                            ];
}
-(void)pushControllerfAtIndex:(NSInteger)index withNavigationController:(UINavigationController*)navigationController {
    
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //    switch (index) {
    //        case 0:{
    //
    //            [navigationController popToRootViewControllerAnimated:YES];
    //
    //            break;
    //        }
    //        case 1:{
    //
    //
    //            MyProfileViewController *myProfile = [storyboard instantiateViewControllerWithIdentifier:@"myProfileViewController"];
    //
    //            myProfile.viewMode = Profile;
    //            [navigationController pushViewController:myProfile animated:YES];
    //
    //            break;
    //        }
    //        case 2:{
    //
    //            CountryInformationViewController *countryInfo = [storyboard instantiateViewControllerWithIdentifier:@"countryInfo"];
    //
    //
    //            [navigationController pushViewController:countryInfo animated:YES];
    //
    //            break;
    //        }
    //        case 3:{
    //
    //            Contact_UsVc *contactUs = [storyboard instantiateViewControllerWithIdentifier:@"contactUs"];
    //
    //            [navigationController pushViewController:contactUs animated:YES];
    //
    //            break;
    //        }
    //        case 4:{
    //
    //            NotificationViewController *notifications = [storyboard instantiateViewControllerWithIdentifier:@"notifications"];
    //
    //            [navigationController pushViewController:notifications animated:YES];
    //
    //
    //            break;
    //        }
    //        case 5:{
    //
    //            MyProfileViewController *myProfile = [storyboard instantiateViewControllerWithIdentifier:@"myProfileViewController"];
    //
    //            myProfile.viewMode = Settings;
    //            [navigationController pushViewController:myProfile animated:YES];
    //
    //            break;
    //        }
    //        case 6:{
    //                            AppDelegate *appDel = [UIApplication sharedApplication].delegate;
    //
    //                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirmation" message:@"Do You Want To Logout ?" preferredStyle:UIAlertControllerStyleAlert];
    //
    //                            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //
    //                                [SessionManager logout];
    //
    //                                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //
    //                                InitialNavigationController *navController = [storyboard instantiateInitialViewController];
    //
    //                                [appDel.window
    //                                 setRootViewController:navController];
    //
    //                                [appDel.window makeKeyAndVisible];
    //
    //                            }];
    //
    //                            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    //                            }];
    //
    //                            [alertController addAction:okAction];
    //                            [alertController addAction:cancelAction];
    //            
    //                            [navigationController.topViewController presentViewController:alertController
    //                                                     animated:YES
    //                                                   completion:nil];
    //                            
    //            break;
    //        }
    //        default:
    //            break;
    //    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;

    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake( 0.0
                                                                 ,0.0
                                                                 ,self.view.frame.size.width
                                                                 ,60)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    UILabel *userLabel = [[UILabel alloc]initWithFrame:CGRectMake( 10
                                                                  ,10
                                                                  ,headerView.frame.size.width-50
                                                                  ,50)];
    [userLabel setFont:[UIFont systemFontOfSize:15]];
    userLabel.text = [NSString stringWithFormat:@"Dr. %@ %@",appDelegate.dashboardData[@"doctor_details"][@"first_name"],appDelegate.dashboardData[@"doctor_details"][@"last_name"]];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake( headerView.frame.size.width - 50
                                                                          ,headerView.frame.size.height/2-20
                                                                          ,40
                                                                          ,40)];
    
    [imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://logintohealth.com/demo/assets/uploads/doctors/%@",appDelegate.dashboardData[@"doctor_details"][@"profile_image"]]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    [imageView.layer setCornerRadius:20];
    [headerView addSubview:imageView];
    [headerView addSubview:userLabel];
    
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 60;
}
@end