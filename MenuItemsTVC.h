//
//  MenuItemsTVC.h
//  ObjCExample
//
//  Created by Evgeny on 09.01.15.
//  Copyright (c) 2015 Evgeny Nazarov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuControllerDelegate <NSObject>

@optional
- (void)menuController:(id)menuController didSelectRow:(NSInteger)row fromMenu:(NSArray *) menu;

@end

@interface MenuItemsTVC : UITableViewController {
    
    NSArray *viewControllers;
}
@property (nonatomic, assign) id < MenuControllerDelegate > delegate;
@property (nonatomic, strong) NSArray * menuArray;
@property (nonatomic, strong) NSArray * imageArray;

// shared instance
+ (instancetype)sharedInstance;

@end
