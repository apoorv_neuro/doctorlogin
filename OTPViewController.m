//
//  OTPViewController.m
//  DoctorLogin
//
//  Created by Hitesh Dhawan on 07/05/16.
//  Copyright © 2016 Apoorv Suri. All rights reserved.
//

#import "OTPViewController.h"
#import <SVProgressHUD.h>
#import <AFNetworking.h>
#import "AppDelegate.h"
#import "CustomAlertController.h"
#import "PersistencyManager.h"
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface OTPViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCheckMark;
@property (weak, nonatomic) IBOutlet UILabel *lblVerificationDeclaration;
@property (weak, nonatomic) IBOutlet UIButton *buttonAgree;
@property (weak, nonatomic) IBOutlet UITextField *txtfldVerificationCode;
@property (weak, nonatomic) IBOutlet UIButton *buttonVerifyMobile;

@end

@implementation OTPViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.imageViewCheckMark.image = [[UIImage imageNamed:@"tick"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.imageViewCheckMark.opaque = YES;
    self.imageViewCheckMark.layer.opaque = YES;
    
    self.lblVerificationDeclaration.text = [NSString stringWithFormat:@"A verification code has been sent to your mobile %@ by SMS",self.mobileNumber];
}

- (IBAction)checkButtonPressed:(UIButton *)sender {
    
    sender.selected = !sender.isSelected;
    
    if (sender.selected && self.txtfldVerificationCode.text.length == 6) {
        
        self.buttonVerifyMobile.enabled = YES;
        
    } else {
        
        self.buttonVerifyMobile.enabled = NO;
    }
}


- (IBAction)bttnVerifyMobileTouched:(id)sender {
    
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentInternetStatus) {
        
        [SVProgressHUD show];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        
        //web_doctor_mobile_verification
        NSString *urlAsString = [NSString stringWithFormat:@"%@web_doctor_mobile_verification",BASE_URL];
        
        NSDictionary *parameters = @{@"doctor_id"  : _doctorId ,
                                     @"verify_code":_txtfldVerificationCode.text
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 [SVProgressHUD dismiss];
                 
                 NSLog(@" Verify Doctor API --- Response---->%@",responseObject);
                 
                 if (responseObject!= nil && responseObject!= NULL) {
                     
                     if ([responseObject[@"message"] isKindOfClass:[NSString class]]) {
                         
                         [CustomAlertController showAlertOnController:self
                                                        withAlertType:Simple
                                                        andAttributes:@{
                                                                        @"title"   : @"",
                                                                        @"message" : responseObject[@"message"]
                                                                        }];
                         
                     }
                     if ([responseObject[@"success"] isKindOfClass:[NSString class]] && [responseObject[@"success"]isEqualToString:@"1"]) {
                        
                         [PersistencyManager loginWithDoctorId:_doctorId];
                         
                         [appDelegate showDashboard];

                     }
                 } else {
                     
                     // When The Server Returns NULL
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
                 
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 
                 [SVProgressHUD dismiss];
                 
                 NSLog(@" Verify Doctor--- Error |||_______---->>>%@",[error localizedDescription]);
             }];
    } else {
        
        [CustomAlertController showAlertOnController : self
                                       withAlertType : Network
                                       andAttributes : nil];
    }
}
- (IBAction)bttnResendCodeTouched:(id)sender {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentInternetStatus) {
        
        [SVProgressHUD show];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSString *urlAsString = [NSString stringWithFormat:@"%@resend_otp_verified",BASE_URL];
        
        NSDictionary *parameters = @{@"doctor_id" : _doctorId ,
                                     };
        
        [manager.responseSerializer setAcceptableContentTypes : [NSSet setWithObject:@"text/html"]];
        
        [manager.requestSerializer  setAuthorizationHeaderFieldWithUsername : @"kushals@neuronimbus.com"
                                                                   password : @"Neuro@123"];
        [manager GET:urlAsString
          parameters:parameters
            progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 
                 [SVProgressHUD dismiss];
                 
                 NSLog(@" ResendCode API --- Response---->%@",responseObject);
                 
                 if (responseObject != nil && responseObject != NULL) {
                     
                     
                 } else {
                     
                     // When The Server Returns NULL
                     
                     [CustomAlertController showAlertOnController:self
                                                    withAlertType:Simple
                                                    andAttributes:@{
                                                                    @"title" : @"No Data Received",
                                                                    @"message" : @"Server Is Not Responding"
                                                                    }];
                 }
                 
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 
                 
                 [SVProgressHUD dismiss];
                 
                 NSLog(@"ResendCode API --- Error |||_______---->>>%@",[error localizedDescription]);
             }];
    } else {
        
        [CustomAlertController showAlertOnController:self
                                       withAlertType:Network
                                       andAttributes:nil];
    }
}

#pragma mark - UItextField Delegate

- (IBAction)textFieldDidChange:(UITextField *)sender {
    
    self.imageViewCheckMark.tintColor = sender.text.length == 6 ? UIColorFromRGB(0x27ae60) : UIColorFromRGB(0x999999);
    
    if (self.buttonAgree.selected && self.txtfldVerificationCode.text.length == 6) {
        
        self.buttonVerifyMobile.enabled = YES;
        
    } else {
        
        self.buttonVerifyMobile.enabled = NO;
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.text.length == 6 && ![string isEqualToString:@""]) return NO;
    
    return YES;
}
- (IBAction)buttonBackPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
